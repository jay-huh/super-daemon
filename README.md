# Super-Daemon for Monitoring Thread Healthy Informations

### Resource Status

```c
typedef enum RESOURCE_STATUS_E
{
    RESOURCE_STATUS_IDLE = 0,
    RESOURCE_STATUS_SLEEP = RESOURCE_STATUS_IDLE,
	RESOURCE_STATUS_WAIT_HEARTBEAT = RESOURCE_STATUS_IDLE,
	RESOURCE_STATUS_ALIVE,
 	RESOURCE_STATUS_END
} RESOURCE_STATUS;

typedef enum SYSTEM_STATUS_E
{
    SYSTEM_STATUS_HEALTHY = 0,
    SYSTEM_STATUS_REBOOT,
    SYSTEM_STATUS_POWER_DOWN,
    SYSTEM_STATUS_END
} SYSTEM_STATUS;

#define RESOURCE_NAME_LEN	( 32 )

#ifndef tid_t
	typedef pid_t tid_t
#endif	//	tid_t

typedef struct RESOURCE_S
{
	pid_t pid;
    tid_t tid;
    char name[ RESOURCE_NAME_LEN ];
    RESOURCE_STATUS status;		//	used in Daemon
    SYSTEM_STATUS msg;			//	command from CDR
    bool isAlive;
    struct timeval lastSendTime;
	long seq;
} RESOURCE;	//	32 + ( 2 * pid_t ) + enum + bool + timwval + long = 60 bytes

typedef struct RESOURCE_NODE_S
{
    struct RESOURCE_NODE *pNext;

    RESOURCE resource;
} RESOURCE_NODE;

typedef struct RESOURCE_LIST_S
{
    RESOURCE_NODE *pHead;
    RESOURCE_NODE *pTail;

	RESOURCE_NODE *pCurrent;
} RESOURCE_LIST;

tyoedef struct MSG_DATA_S
{
    long mtype;		//	must need for msg queue

    RESOURCE resource;
} MSG_DATA;
```



## Sequence Diagram

### Message Queue Operation

```mermaid
%% Example of sequence diagram
sequenceDiagram

participant Watchdog
participant Daemon
participant CDR
participant T_1
%%participant T_2

note over Daemon, CDR: Create Msg. Queue

Daemon ->> Watchdog: Initialize
Daemon ->> +CDR: fork() and exec()
CDR -->> -Daemon: return

activate CDR
CDR ->> +T_1: CreateThread()

T_1 ->> -Daemon: Send Msg( "IDLE" )
activate Daemon
	Daemon ->> Daemon: Register T_1
deactivate Daemon

%%CDR ->> +T_2: CreateThread()
%%T_2 ->> -Daemon: Send Msg( "IDLE" )
%%activate Daemon
%%	Daemon ->> Daemon: Register T_2
%%deactivate Daemon

note over Daemon, T_1: Send Thread Status every second
activate T_1
%%activate T_2

loop every second
	alt Thread Status is PAUSE
		T_1 ->> Daemon: Send Msg( "PAUSE" )
		activate Daemon
			Daemon ->> Daemon: Update T_1 status to SLEEP
		deactivate Daemon
		note over Daemon, T_1: Except T_1 in check list
	else Thread Status is EXIT
		T_1 ->> Daemon: Send Msg( "EXIT" )
		activate Daemon
			Daemon ->> Daemon: Update T_1 status to SLEEP. and Deregister T_1
		deactivate Daemon
		note over Daemon, T_1: Except T_1 in check list
	else Thread Status is RUNNING
			T_1 ->> Daemon: Send Msg( "HEALTHY" )
		activate Daemon
			Daemon ->> Daemon: Update T_1 status to ALIVE
			%%T_2 ->> Daemon: Send Msg( "HEALTHY" )
			%%Daemon ->> Daemon: Update T_2 status to ALIVE
		deactivate Daemon

		note over Daemon, T_1: Check thread status in check list
		alt ALL_ALIVE
			Daemon ->> Watchdog: KEEP_ALIVE
			note over Daemon, CDR: Clear Alive status
		else
			note over Watchdog, Daemon: Check WDT remain time.
			note over Watchdog, CDR: Remain under 1 second, update status file. Expect WDT Reboot.
		end		%% ALL_ALIVE
	end		%%	Thread Status

end		%%	loop

CDR ->> CDR: DeinitializeThreads()
activate CDR
	CDR ->> +T_1: JoinThread()
	T_1 -->> -CDR: return
	deactivate T_1
	%%deactivate T_2
deactivate CDR

CDR->>CDR: exit
deactivate CDR

```

### Reboot and Powerdown

```mermaid
%% Example of sequence diagram
sequenceDiagram

participant Watchdog
participant Daemon
participant CDR
participant T_1
%%participant T_2

note over Daemon: Get Status from file
%%note over Daemon, CDR: Create Msg. Queue

%%Daemon ->> Watchdog: Initialize
%%Daemon ->> +CDR: fork() and exec()
%%CDR -->> -Daemon: return

activate CDR
CDR ->> +T_1: CreateThread()

%%CDR ->> +T_2: CreateThread()
%%T_2 ->> -Daemon: Send Msg( "IDLE" )
%%activate Daemon
%%	Daemon ->> Daemon: Register T_2
%%deactivate Daemon

note over Daemon, T_1: Send Thread Status every second
activate T_1
%%activate T_2

loop every second
	alt Thread Status is RUNNING
			T_1 ->> Daemon: Send Msg( <MSG> )
		alt MSG == HEALTHY
			activate Daemon
				Daemon ->> Daemon: Update T_1 status to ALIVE
				%%T_2 ->> Daemon: Send Msg( "HEALTHY" )
				%%Daemon ->> Daemon: Update T_2 status to ALIVE
			deactivate Daemon
		else MSG == REBOOT or POWER_DOWN
		note over Daemon, CDR: Known Reboot or Powerdown
        	activate Daemon
				Daemon ->> Daemon: set System Status to REBOOT or POWER_DOWN and exit loop
			deactivate Daemon
		end
		note over Daemon, T_1: Check thread status in check list
		alt ALL_ALIVE
			Daemon ->> Watchdog: KEEP_ALIVE
			note over Daemon, CDR: Clear Alive status
		else Some Threads are Pending
			note over T_1: Deadlock occurred
			note over Watchdog, T_1: Check WDT remain time
			note over Watchdog, CDR: Remain under 1 second, update status file. Expect WDT Reboot.
			note over Watchdog, CDR: EXPECT: Unknown Reboot by WDT
			activate Daemon
				note over Watchdog, CDR: POWER_DOWN: Reboot count over 3-times by WDT
				Daemon ->> Daemon: set System Status to REBOOT or POWER_DOWN and exit loop
			deactivate Daemon
		end		%% ALL_ALIVE
	end		%%	Thread Status
end		%%	loop

Daemon ->> CDR: Stop
CDR -->> Daemon: return
deactivate CDR

note over Watchdog, T_1: Update status file

alt SYSTEM_STATUS == REBOOT
	Daemon ->> Daemon: Update Reboot count and Reboot
else SYSTEM_STATUS == POWER_DOWN
	Daemon ->> Daemon: Clear Reboot count and Power-down
end


```

