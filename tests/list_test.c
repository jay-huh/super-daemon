#include	"list.h"

//#include	<stdio.h>
#include	<stdlib.h>		/* for convenience */

#include	"mutex.h"

#include	"err.h"
#include	"debug.h"

#define		TEST_NODE		256

static pthread_mutex_t	_Mutex = PTHREAD_MUTEX_INITIALIZER;

/*	INFO: thread function for list test
 *	by jyhuh 2019-03-01 19:14:13	*/
void * ThreadTest( void *arg )
{
	LIST	*pList = ( LIST * )( arg );
	NODE	*pNode = NULL;
	tid_t	tid = gettid();
	static long idx = 0;

	assert( pList != NULL );

	pNode = malloc( sizeof( NODE ) );
	if( pNode == NULL )
	{
		ERROR_PRINTF( "malloc() failed!!\n" );
		return 0;
	}

	//INFO_PRINTF( "Thread: ppid: %d pid: %d tid: %ld\n", getppid(), getpid(), gettid() );

	pNode->pNext = NULL;
	pNode->key = tid % 10;

	pNode->resource.seq = ( long )( idx++ );
	pNode->resource.pid = getpid();
	pNode->resource.tid = tid;
	snprintf( pNode->resource.name, RESOURCE_NAME_LEN, "T_%ld", pNode->resource.seq );

	if( AddNode( pList, pNode ) == false )
	{
		ERROR_PRINTF( "THREAD: AddNode() failed!!\n" );
	}
	free( pNode );

	usleep( 10 * 1000 );

	{
		MutexLock( &_Mutex );

		NODE *pIter = NULL;
		int i = 0;

		//for( pIter = IterBegin( pList ) ; pIter != IterEnd( pList ) ; pIter = IterNext( pList, pIter ) )
		for( pIter = IterBegin( pList ) ; pIter != NULL ; pIter = IterNext( pList, pIter ) )
		{
			INFO_YPRINTF( "%d ---Iter: %p pNext: %p count: %d\n", i++, pIter, IterNext( pList, pIter ), GetNodeCount( pList ) );
		}
		MutexUnlock( &_Mutex );

	}

	if( ( tid % 4 ) == 2 )
	{
		DeleteNodeByField( pList, RESOURCE_FIELD_TID, &tid );
	}
	//PrintlList( pList );

	return ( 0 );
}

void Test( void )
{
	LIST	list;
	int		i = 0;
	int err;

	pthread_t	tid[ TEST_NODE ];

	memset( tid, 0x0, sizeof( pthread_t ) * TEST_NODE );

	InitList( &list );

	for( i = 0 ; i < TEST_NODE ; i += 4 )
	{
		//DEBUG_PRINTF( "THREAD_CREATE %d:\n", i );
		err = pthread_create( &tid[ i ], NULL, ThreadTest, &list );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 1 ], NULL, ThreadTest, &list );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 2 ], NULL, ThreadTest, &list );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 3 ], NULL, ThreadTest, &list );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		//usleep( 1 * 1000 );
	}

	//sleep( 3 );

	for( i = 0 ; i < TEST_NODE ; i++ )
	{
		//usleep( 10 * 1000 * i );
		pthread_join( tid[ i ] , NULL );
	}
	DEBUG_PRINTF( "join %d\n", i );

	for( i = 0 ; i < TEST_NODE ; i += 2 )
	{
		//DeleteNodeByTest( &list, i );
	}
	PrintlList( &list );
	DeinitList( &list );

	PrintlList( &list );
}

int main( int argc, char *argv[] )
{
	Test();

	exit( EXIT_SUCCESS );
}

