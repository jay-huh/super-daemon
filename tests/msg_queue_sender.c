//#include	<stdio.h>
#include	<stdlib.h>		/* for convenience */
#include	<pthread.h>

#include	<sys/ipc.h>
#include	<sys/msg.h>
#include	<sys/time.h>	//	gettimeofday

#include	"msg_queue.h"

#include	"err.h"
#include	"debug.h"

#define		MAX_SENDER		4

void * ThreadSender( void *arg )
{
	int *pCount = ( int * )arg;
	int seq = 0;

	int msgId = -1;
	key_t key = -1;
	char name[ MSG_QUEUE_NAME_LEN ] = { 0x0, };

	MSG_DATA_RESOURCE	msgDataResource = { -1, RESOURCE_INITIALIZER };
	int count = 0;

	key = GenerateKey( NULL, MSG_QUEUE_MAGIC_KEY );
	if( key < 0 )
	{
		ERROR_PRINTF( "GenerateKey() failed. errno: %d %s\n", errno, strerror( errno ) );
		return 0;
	}

	msgId = GetMsgQueue( key );

	INFO_PRINTF( "SENDER | msgId: %d Count: %d\n", msgId, *pCount );

	{
		msgDataResource.msgType = getpid();

		msgDataResource.resource.type = RESOURCE_TYPE_THREAD;
		msgDataResource.resource.pid = getpid();
		msgDataResource.resource.tid = gettid();
		snprintf( msgDataResource.resource.name, RESOURCE_NAME_LEN, "SENDER_%ld", gettid() );
		snprintf( name, RESOURCE_NAME_LEN, "SENDER_%ld", gettid() );

		msgDataResource.resource.status = RESOURCE_STATUS_REGISTER;
		//msgDataResource.resource.command = SYSTEM_CMD_NONE;
		msgDataResource.resource.isAlive = true;
		gettimeofday( &msgDataResource.resource.lastSendTime, NULL/* tz */ );
		msgDataResource.resource.seq = 0;
	}

	while( 1 )
	{
		sleep( 1 );

		if( count >= *pCount )
		{
			count = -1;
		}
		if( count < 0 ) break;;

		/*	INFO: skip SendMessage test	*/
#if 0
		if( ( ( count + gettid() ) % 4 ) == 0 )
		{
			count++;
			continue;
		}
#endif
		gettimeofday( &msgDataResource.resource.lastSendTime, NULL/* tz */ );
		if( SendMessage( msgId,&msgDataResource, sizeof( MSG_DATA_RESOURCE ) ) == false )
		{
			ERROR_PRINTF( "---\n" );
			break;
		}
		count++;

		msgDataResource.resource.status = RESOURCE_STATUS_ALIVE;
		msgDataResource.resource.seq++;

		{
			seq = msgDataResource.resource.seq;
			if( ( seq % 4 ) == ( gettid() % 4 ) )
			{
				msgDataResource.resource.status++;
				msgDataResource.resource.status =( msgDataResource.resource.status % RESOURCE_STATUS_END );
				if( msgDataResource.resource.status == RESOURCE_STATUS_COMMAND )
				{
					msgDataResource.resource.command = ( msgDataResource.resource.tid % SYSTEM_COMMAND_END );
					INFO_PRINTF( "SENDER | \'%s\' msgId: %d seq: %d command: %d\n", \
							name, msgId, seq, msgDataResource.resource.command );
				}
				INFO_PRINTF( "SENDER | \'%s\' msgId: %d seq: %d status: %d\n", \
						name, msgId, seq, msgDataResource.resource.status );
			}
		}
	}
	INFO_PRINTF( "SENDER | EXIT msgId: %d Count: %d\n", msgId, *pCount );

	//RemoveMsgQueueByKey( key );

	return ( 0 );
}

void Test( void )
{
	int		i = 0;
	int err;
	int		count[ MAX_SENDER ] = { 0, };
	//key_t key = -1;

	pthread_t	tid[ MAX_SENDER ];
	//char name[ MSG_QUEUE_NAME_LEN ] = { 0x0, };

	memset( tid, 0x0, sizeof( pthread_t ) * MAX_SENDER );

	InitMsgQueue();

	for( i = 0 ; i < MAX_SENDER ; i++ )
	{
		count[ i ] = 10;
	}

	//count[ 0 ] = 100;

#if 1
	for( i = 0 ; i < MAX_SENDER ; i++ )
	{
		err = pthread_create( &tid[ i ], NULL, ThreadSender, &count[ i ]);
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
	}
#else
	for( i = 0 ; i < MAX_SENDER ; i += 4 )
	{
		err = pthread_create( &tid[ i ], NULL, ThreadSender, &count[ i ]);
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 1 ], NULL, ThreadSender, &count[ i + 1 ] );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 2 ], NULL, ThreadSender, &count[ i + 2 ] );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
		err = pthread_create( &tid[ i + 3 ], NULL, ThreadSender, &count[ i + 3 ] );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
	}
#endif

	//sleep( 3 );

	for( i = 0 ; i < MAX_SENDER ; i++ )
	{
		pthread_join( tid[ i ] , NULL );
	}
	DEBUG_PRINTF( "join %d\n", i );

	//DeinitMsgQueue();
}

int main( int argc, char *argv[] )
{
	Test();

	exit( EXIT_SUCCESS );
}

