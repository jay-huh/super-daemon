//#include	<stdio.h>
#include	<stdlib.h>		/* for convenience */
#include	<pthread.h>
#include	<fcntl.h>

#include	<sys/ipc.h>
#include	<sys/msg.h>

#include	"msg_queue.h"

#include	"err.h"
#include	"debug.h"

#ifndef KEY_BASE_FILE
	#define	KEY_BASE_FILE	"/tmp/keybase"
#endif /* ifndef KEY_BASE_FILE */

#define		MAX_RECEIVER	4

void * ThreadReceiver( void *arg )
{
	char *keyfile = KEY_BASE_FILE;
	int msgId = -1;
	key_t key = -1;
	char name[ MSG_QUEUE_NAME_LEN ] = { 0x0, };

	static SYSTEM_STATUS		s_status = SYSTEM_STATUS_HEALTHY;
	SYSTEM_COMMAND		command = SYSTEM_CMD_NONE;
	MSG_DATA_RESOURCE	msgDataResource;
	ssize_t				len;
	int					messages = -1;
	int i = 0;
	long seq;
	int count = 0;

	snprintf( name, MSG_QUEUE_NAME_LEN, "MSGQ_%d", 1004 );

	if( access( keyfile, F_OK ) < 0 )
	{
		INFO_YPRINTF( "keyfile( \"%s\" Not Found ==> Create file...\n", keyfile );
		int fd = creat( keyfile, S_IRUSR | S_IWUSR );
		if( fd < 0 )
		{
			ERROR_PRINTF( "creat(\"%s\") failed. errno: %d %s\n", keyfile, errno, strerror( errno ) );
			return 0;
		}
		close( fd );
	}

	key = ftok( keyfile, MSG_QUEUE_MAGIC_KEY );
	if( key < 0 )
	{
		ERROR_PRINTF( "ftok() failed. errno: %d %s\n", errno, strerror( errno ) );
	}
	INFO_YPRINTF( "KEY: file \'%s\' Magic: \'%c\' key: %d\n", keyfile, MSG_QUEUE_MAGIC_KEY, key );

	msgId = CreateMsgQueue( name, MSG_QUEUE_TYPE_SYSV, MSG_DATA_TYPE_RESOURCE, key, 512 );

	INFO_PRINTF( "MAIN | msgId: %d\n", msgId );

	while( 1 )
	{
		int cnt = 0;

		messages = -1;

		while( ( messages < 0 ) && ( cnt < 10 ) )
		{
			messages = GetNumberOfMessages( msgId );
			if( cnt++ >= 10 )
			{
				INFO_PRINTF( "MAIN | msgId: %d | msgs %d count: %d cnt: %d\n", msgId, messages, count, cnt );
				cnt = 0;
			}
			usleep( 100 * 1000 );
		}

		for( i = 0 ; i < messages ; i++ )
		{
			len = ReceiveMessage( msgId, &msgDataResource,
					sizeof( MSG_DATA_RESOURCE ), 0, IPC_NOWAIT | MSG_NOERROR );
			if( len < 0 )
			{
				//ERROR_PRINTF( "msgrcv() failed. errno: %d %s\n", errno, strerror( errno ) );
				continue;
			}
			count++;
			seq = msgDataResource.resource.seq;
			if( ( count % 1000 ) == 0 )
			{
				INFO_YPRINTF( "RECV: \'%s\' msgId: %d | seq: %ld i: %d\n",
						msgDataResource.resource.name, msgId, msgDataResource.resource.seq, i );
			}

			command =  msgDataResource.resource.command;

			if( command == SYSTEM_CMD_POWER_DOWN )
			{
				s_status =  msgDataResource.resource.command;
				WARN_PRINTF( "RECV: POWER_DOWN \'%s\' msgId: %d | seq: %ld i: %d\n",
					msgDataResource.resource.name, msgId, msgDataResource.resource.seq, i );
				break;
			}
			if( seq == 100 ) continue;	//	none
		}
		if( s_status == SYSTEM_STATUS_REBOOT || s_status == SYSTEM_STATUS_POWER_DOWN )
		{
			break;
		}
	}
	messages = GetNumberOfMessages( msgId );
	WARN_PRINTF( "RECV: LAST messages: %d\n", messages );

	//RemoveMsgQueueByKey( key );

	return ( 0 );
}

void Test( void )
{
	int		i = 0;
	int err;

	pthread_t	mainTid[ MAX_RECEIVER ];
	//char name[ MSG_QUEUE_NAME_LEN ] = { 0x0, };

	InitMsgQueue();

	for( i = 0 ; i < MAX_RECEIVER ; i++ )
	{
		err = pthread_create( &mainTid[ i ], NULL, ThreadReceiver, NULL );
		if( err != 0 )
		{
			err_exit(err, "can't create thread");
		}
	}

	for( i = 0 ; i < MAX_RECEIVER ; i++ )
	{
		pthread_join( mainTid[ i ], NULL );
	}
	DEBUG_PRINTF( "join %d\n", i );

	DeinitMsgQueue();
}

int main( int argc, char *argv[] )
{
	Test();

	exit( EXIT_SUCCESS );
}

