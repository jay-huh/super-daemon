#ifndef __MSG_QUEUE_H__
#define __MSG_QUEUE_H__

#include	<stdbool.h>

#include	<sys/types.h>

#include	"resource_msg.h"

typedef enum MSG_QUEUE_TYPE_E
{
	MSG_QUEUE_TYPE_BEGIN = -1,
	MSG_QUEUE_TYPE_UNUSED = MSG_QUEUE_TYPE_BEGIN,
	MSG_QUEUE_TYPE_SYSV = 0,
	MSG_QUEUE_TYPE_POSIX,

	MSG_QUEUE_TYPE_END
} MSG_QUEUE_TYPE;

typedef enum MSG_DATA_TYPE_E
{
	MSG_DATA_TYPE_BEGIN = 0,

	MSG_DATA_TYPE_INT = MSG_DATA_TYPE_BEGIN,
	MSG_DATA_TYPE_LONG,
	MSG_DATA_TYPE_UNKNOWN,
	MSG_DATA_TYPE_RESOURCE,

	MSG_DATA_TYPE_END
} MSG_DATA_TYPE;

#define	MSG_QUEUE_NAME_LEN	( 32 )

typedef struct MSG_QUEUE_S
{
	MSG_QUEUE_TYPE	type;
	MSG_DATA_TYPE	msgDataType;
	char			name[ MSG_QUEUE_NAME_LEN ];

	key_t			key;
	int				id;
	size_t			msgMaxSize;

	/*	TODO: Add semaphore	*/
} MSG_QUEUE;

/*	NOTE: Message Queue Pool
 *	*/

#define	MSG_QUEUE_MAGIC_KEY		'm'
#define MAX_KEY_FILE_PATH_LEN	( 128 )

#define DEFAULT_KEY_FILE_NAME	"/tmp/keyfile"

#define	MAX_MSG_QUEUE_LIMIT		( 16 )
#define	MSG_DATA_MAX_SIZE		( 1024 )

#define	MSG_TYPE_ALL			( 0 )

#define	DEFAULT_MSG_QUEUE_TYPE	MSG_QUEUE_TYPE_SYSV
#define	DEFAULT_MSG_DATA_TYPE	MSG_DATA_TYPE_INT
#define DEFAULT_MSG_QUEUE_NAME	"MSG_Q_UNDEF"

#define MSG_QUEUE_INITIALIZER \
	{ \
		.type = DEFAULT_MSG_QUEUE_TYPE, \
		.msgDataType = DEFAULT_MSG_DATA_TYPE, \
		.name = DEFAULT_MSG_QUEUE_NAME, \
		.key = -1, \
		.id = -1, \
		.msgMaxSize = MSG_DATA_MAX_SIZE \
	}

extern const key_t GenerateKey( const char * const pKeyFile, const int projId );

extern const bool InitMsgQueue( void );
extern const bool DeinitMsgQueue( void );

/*	return msgid	*/
extern const int CreateMsgQueue(
		const char * const pName,
		const MSG_QUEUE_TYPE type,
		const MSG_DATA_TYPE msgDataType,
		const key_t key,
		const size_t msgMaxSize );

extern const int GetMsgQueue( const key_t key );

extern const bool RemoveMsgQueue( const int msgId );
extern const bool RemoveMsgQueueByKey( const key_t key );

extern const bool SendMessage(
		const int msgId,
		const void * const pMsg,
		const size_t size );

extern const ssize_t ReceiveMessage(
		const int msgId,
		void * const pMsg,
		const size_t size,
		const long msgType,
		const int msgFlag );

const int GetNumberOfMessages( const int msgId );
#endif /* ifndef __MSG_QUEUE_H__ */
