#ifndef __LIST_H__
#define __LIST_H__

#include	"resource_list.h"

extern const bool InitList( LIST * const pList );
extern const bool DeinitList( LIST * const pList );

extern const bool IsListEmpry( LIST * const pList );

extern const bool AddNode( LIST * const pList, NODE * const pNode );
extern const bool DeleteNode( LIST * const pList, const NODE * pNode );
extern const bool UpdateNode( LIST * const pList, const NODE * const pNode );

extern NODE * const FindNode( LIST * const pList, const NODE * const pNode );
extern NODE * const FindNodeByKey( LIST * const pList, const unsigned int key );

extern NODE * const IterBegin( LIST * const pList );
extern NODE * const IterEnd( LIST * const pList );
extern NODE * const IterNext( LIST * const pList, const NODE * const pNode );

extern const int GetNodeCount( const LIST * const pList );
extern const bool PrintlList( LIST * const pList );
#endif /* ifndef __LIST_H__ */
