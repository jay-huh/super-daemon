#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#include	<stdbool.h>
#include	<sys/types.h>
#include	<sys/time.h>

typedef enum RESOURCE_TYPE_E
{
	RESOURCE_TYPE_PROCESS = 0,
	RESOURCE_TYPE_THREAD,

	RESOURCE_TYPE_END
} RESOURCE_TYPE;

typedef enum RESOURCE_STATUS_E
{
	RESOURCE_STATUS_IDLE = 0,
	RESOURCE_STATUS_SLEEP = RESOURCE_STATUS_IDLE,
	RESOURCE_STATUS_REGISTER = 1,
	RESOURCE_STATUS_ALIVE,
	RESOURCE_STATUS_WAIT_HEARTBEAT = RESOURCE_STATUS_ALIVE,
	RESOURCE_STATUS_COMMAND,

	RESOURCE_STATUS_DEREGISTER,

	RESOURCE_STATUS_END
} RESOURCE_STATUS;

typedef enum SYSTEM_STATUS_E
{
	SYSTEM_STATUS_HEALTHY = 0,
	SYSTEM_STATUS_REBOOT,
	SYSTEM_STATUS_POWER_DOWN,
	SYSTEM_STATUS_FORCE_POWER_DOWN,

	SYSTEM_STATUS_END,

	SYSTEM_CMD_NONE = SYSTEM_STATUS_HEALTHY,
	SYSTEM_CMD_REBOOT = SYSTEM_STATUS_REBOOT,
	SYSTEM_CMD_POWER_DOWN = SYSTEM_STATUS_POWER_DOWN,
	SYSTEM_CMD_CLEAR_REBOOT_COUNT,

	SYSTEM_COMMAND_END = SYSTEM_STATUS_END
} SYSTEM_STATUS;

typedef SYSTEM_STATUS	SYSTEM_COMMAND;

#define RESOURCE_NAME_LEN	( 32 )

#ifndef tid_t
	typedef pid_t tid_t;
#endif	//	tid_t

typedef struct RESOURCE_S
{
	char				name[ RESOURCE_NAME_LEN ];
	pid_t				pid;
	tid_t				tid;

	RESOURCE_TYPE		type;
	RESOURCE_STATUS		status;		//	used in daemon
	SYSTEM_COMMAND		command;	//	command from application
	bool				isAlive;

	struct timeval		lastSendTime;
	long				seq;
} RESOURCE;	//	32 + ( 2 * pid_t ) + ( 3 * enum ) + bool + timeval + long = 68 bytes

#define DEFAULT_RESOURCE_NAME	"RESOURCE_UNDEF"
#define RESOURCE_INITIALIZER \
	{ \
		.name = DEFAULT_RESOURCE_NAME, \
		.pid = -1, \
		.tid = -1, \
		.type = RESOURCE_TYPE_PROCESS, \
		.status = RESOURCE_STATUS_IDLE, \
		.command = SYSTEM_CMD_NONE, \
		.isAlive = false, \
		.lastSendTime = ( struct timeval ){ 0 }, \
		.seq = 0 \
	}

typedef enum RESOURCE_FIELD_E
{
	RESOURCE_FIELD_PID = 0,
	RESOURCE_FIELD_TID,
	RESOURCE_FIELD_STATUS,

	RESOURCE_FIELD_END
} RESOURCE_FIELD;

#endif /* ifndef __RESOURCE_H__ */
