#ifndef __RESOURCE_MSG_H__
#define __RESOURCE_MSG_H__

#include	"resource.h"

typedef struct MSG_DATA_RESOURCE_S
{
	long msgType;

	RESOURCE resource;
} MSG_DATA_RESOURCE;

#endif /* ifndef __RESOURCE_MSG_H__ */
