#ifndef __DAEMON_H__
#define __DAEMON_H__

#include	<stdbool.h>
#include	<sys/types.h>

#define CH_ROOT_PATH	"/tmp/"
#define DAEMON_PID_FILE "/var/run/daemon_test.pid"
#define LOCK_MODE ( S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH )

#define DEFAULT_DAEMON_NAME		"daemon"
#define MAX_DAEMON_NAME_LEN		( 32 )

typedef	void *( *pfnThreadFunc )( void * );
typedef	void ( *pfnSignalHandler )( int );

typedef struct DAEMON_S
{
	pid_t		pid;
	pthread_t	tid;	//	pthread tid for signal handling
	char		name[ MAX_DAEMON_NAME_LEN ];
	bool		running;

	pthread_t	sigTid;
	sigset_t	sigMask;

	pfnThreadFunc	signalHandler;
} DAEMON;

#define DAEMON_INITIALIZER \
	{ \
		.pid = -1, \
		.name = DEFAULT_DAEMON_NAME, \
		.running = false, \
		.signalHandler = NULL, \
	}

extern const bool InitDaemon( DAEMON * const pDaemon, const char * const pCmd, pfnThreadFunc signalHandler );
extern const bool StartSignalHandler( DAEMON * const pDaemon );
extern bool StopSignalHandler( DAEMON * const pDaemon, void * const pRetVal );

extern const bool SetSignalHandler( const int sigNo, pfnSignalHandler signalHandler );
extern const bool SetSigMask( const int sigNo );

extern const bool IsRunning( const DAEMON * const pDaemon );
extern void SetRunning( DAEMON * const pDaemon, const bool set );

#endif //	__DAEMON_H__
