#ifndef __SUNXI_H__
#define __SUNXI_H__

#include	<stdbool.h>
#include	<linux/types.h>

/** PMIC structure */
typedef struct pmic_rw {
	unsigned char addr;
	unsigned char data;
} PMIC_RW;

/** PMIC INFO */
typedef struct pmic_info {
	int		vac;
	int		iac;
	int 	vbat;
	int 	ibat;
	int 	disvbat;
	int 	disibat;
	int		rest_vol;
	int		ic_temp;
	int 	ac_det;
	int 	ac_valid;
	int 	carbat;
	int 	rev0;
	int 	rev1;
} PMIC_INFO;

#define SUNXI_SET_PMIC				_IOW( 'S', 50, struct pmic_rw )
#define SUNXI_GET_PMIC				_IOR( 'S', 51, struct pmic_rw )
#define SUNXI_GET_PMIC_INFO			_IOR( 'S', 52, struct pmic_info )
#define SUNXI_SET_CACHE_CONTROL		_IOW( 'S', 53, int )

#define WDT_TIMEOUT		( 10 )

extern const int OpenDevice( const char * const pDevFile, const int flags );

extern const bool InitWatchdog( void );
extern const bool CloseWatchdog( void );

extern const bool KeepAliveWatchdog( void );
extern const int GetWatchdogLeftTime( void );

extern const bool InitPmic( void );
extern const bool ClosePmic( void );

extern const bool IsAccValid( void );
extern const bool PmicShutdown( const bool checkAccIn );

#endif /* ifndef __SUNXI_H__ */
