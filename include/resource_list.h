#ifndef __RESOURCE_LIST_H__
#define __RESOURCE_LIST_H__

#include	<stdbool.h>
#include	<pthread.h>

#include	"resource.h"

typedef struct RESOURCE_NODE_S
{
	struct RESOURCE_NODE_S	*pNext;

	unsigned int			key;		//	tid
	union
	{
		char				data[ sizeof( RESOURCE ) ];
		RESOURCE			resource;
	};
} RESOURCE_NODE;

#define RESOURCE_NODE_INITIALIZER \
	{ \
		.pNext = NULL, \
		.key = -1, \
		.resource = RESOURCE_INITIALIZER \
	}

typedef struct RESOURCE_LIST_S
{
	RESOURCE_NODE	*pHead;
	RESOURCE_NODE	*pTail;

	long			count;
	pthread_mutex_t		mutex;
} RESOURCE_LIST;

typedef struct MSG_DATA_S
{
	long mtype;

	RESOURCE resource;
} MSG_DATA;

typedef RESOURCE_LIST	LIST;
typedef RESOURCE_NODE	NODE;

#define NODE_DATA_SIZE		( sizeof( NODE ) - sizeof( RESOURCE_NODE * ) - sizeof( unsigned int ) )

NODE * const FindNodeByField( LIST * const pList, const RESOURCE_FIELD field, const void * const pData );
const bool DeleteNodeByField( LIST * const pList, const RESOURCE_FIELD field, const void * const pData );

#endif	//	__RESOURCE_LIST_H__
