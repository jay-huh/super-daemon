#ifndef __DAEMON_CDR_H__
#define __DAEMON_CDR_H__

#include	"daemonize.h"
#include	"msg_queue.h"
#include	"list.h"

#define	MAX_TASK				( 8 )
#define	MAX_RECEIVER			( 1 )

#define	TIME_SEC_EXPECT_WDT			( 6 )
#define	TIME_SEC_CLEAR_REBOOT_COUNT	( 30 )

typedef struct DAEMON_CDR_S
{
	DAEMON			daemon;

	SYSTEM_STATUS	systemStatus;

	LIST			processList;
	LIST			threadList;

	pid_t			taskPid[ MAX_TASK ];
	pthread_t		msgTid[ MAX_RECEIVER ];
	pthread_t		aliveTid;
	int				alertCount;
} DAEMON_CDR;

extern DAEMON_CDR * const GetDaemonCdr( void );
extern DAEMON_CDR * const InitDaemonCdr( const char * const pCmd );
extern const SYSTEM_STATUS DeinitDaemonCdr( void );

extern const bool IsTaskRunning( void );
extern void SetTaskRunning( const bool set );

extern void SetSystemStatus( const SYSTEM_STATUS status );
extern const SYSTEM_STATUS GetSystemStatus( void );

extern const bool StartReceiver( pthread_t tid[] );
extern const bool StopReceiver( pthread_t tid[] );

extern void * ThreadSignalHandler( void *arg );
extern void * ThreadReceiver( void *arg );
extern void * ThreadAlive( void *pArg );

extern void RegisterTaskPid( const pid_t pid );
extern void DeregisterTaskPid( const pid_t pid );

extern const bool RegisterResource( const RESOURCE * const pResource );
extern const bool SleepResource( const RESOURCE * const pResource );
extern const bool AliveResource( const RESOURCE * const pResource );
extern const bool ParseMessage( const MSG_DATA_RESOURCE * const pMessage );

extern void RebootSystem( void );
extern void ShutdownSystem( void );
#endif /* ifndef __DAEMON_CDR_H__ */
