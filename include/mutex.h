#ifndef __MUTEX_H__
#define __MUTEX_H__

#include	<stdbool.h>
#include	<pthread.h>

extern const bool InitMutex( pthread_mutex_t * const pMutex );
extern const bool DestroyMutex( pthread_mutex_t * const pMutex );

extern const bool MutexLock( pthread_mutex_t * const pMutex );
extern const bool MutexTryLock( pthread_mutex_t * const pMutex );
extern const bool MutexTimedLock( pthread_mutex_t * const pMutex, const int timeout );
extern const bool MutexUnlock( pthread_mutex_t * const pMutex );

#endif /* ifndef __MUTEX_H__ */
