#ifndef __REBOOT_FILE_H__
#define __REBOOT_FILE_H__

#include	<stdbool.h>

/*	Reboot status	*/
#ifdef	__USE_DAEMON_SHUTDOWN__
#define REBOOT_STATUS_FILE				"/system/config/reboot_status"
#else	//	__USE_DAEMON_SHUTDOWN__
#define REBOOT_STATUS_FILE				"/tmp/reboot_status"
#endif	//	__USE_DAEMON_SHUTDOWN__

#define	REBOOT_COUNT_POWER_OFF			( 3 )
#define	REBOOT_COUNT_FORCED_DISABLE		( -100 )

typedef enum REBOOT_REASON_E
{
	REBOOT_BY_NORMAL = 0,
	REBOOT_BY_WATCHDOG,
	REBOOT_BY_FRONT_CAPTURE_ERR,
	REBOOT_BY_REAR_CAPTURE_ERR,

	REBOOT_BY_END,

	REBOOT_BY_ALL = 128
} REBOOT_REASON;

typedef enum REBOOT_REASON_CMD_E
{
	REBOOT_REASON_CMD_CLEAR = 0,
	REBOOT_REASON_CMD_INCREASE,
	REBOOT_REASON_CMD_GET_COUNT,

	REBOOT_REASON_CMD_END
} REBOOT_REASON_CMD;

typedef struct REBOOT_STATUS_S
{
	bool			valid;
	bool			updated;
	REBOOT_REASON	lastReason;
	int				count[ REBOOT_BY_END ];
} REBOOT_STATUS;

//extern const int GetRebootStatusFile( REBOOT_STATUS * const pStatus );
extern const int GetRebootStatusFile( void );
extern const bool UpdateRebootStatusFile( const REBOOT_REASON reason, const REBOOT_REASON_CMD cmd );
extern const bool IsUpdatedRebootStatusFile( void );
extern const REBOOT_REASON GetLastRebootReason( void );

extern const char * ReasonString( const REBOOT_REASON reason );
extern const int GetRebootCount( const REBOOT_REASON reason );
#endif /* ifndef __REBOOT_FILE_H__ */
