#ifndef __DEBUG_H__
#define __DEBUG_H__

#include	<stdio.h>
#include	<unistd.h>		/* for convenience */

#include	<assert.h>

#include	<sys/time.h>	//	timeval

#ifdef __cplusplus
extern "C" {
#endif	//	__cplusplus

#define STR_TIME_STAMP_LEN	( 32 )	//	YYYY-MM-DD HH:MM:SS.xxxxxx => 26 + 1( NULL )

extern long int gettid( void );
extern void time_stamp( void );

extern void MakeDateTimeString( const struct timeval * const pTv, char *pStrTime );
extern void MakeTimeStamp( const struct timeval * const pTv, char *pStrTime );

#ifdef __cplusplus
}
#endif	//	__cplusplus

#define		BLACK			"\033[0;33m"
#define		RED				"\033[0;31m"
#define		GREEN			"\033[0;32m"
#define		YELLOW			"\033[0;33m"
#define		BLUE			"\033[0;34m"
#define		MAGENTA			"\033[0;35m"
#define		CYAN			"\033[0;36m"
#define		WHITE			"\033[0;37m"

#define		BLACK_BOLD		"\033[1;33m"
#define		RED_BOLD		"\033[1;31m"
#define		GREEN_BOLD		"\033[1;32m"
#define		YELLOW_BOLD		"\033[1;33m"
#define		BLUE_BOLD		"\033[1;34m"
#define		MAGENTA_BOLD	"\033[1;35m"
#define		CYAN_BOLD		"\033[1;36m"
#define		WHITE_BOLD		"\033[1;37m"

#define		ENDCOLOR		"\033[0m"

#ifdef	__DEBUG__
#define	DEBUG_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)]: " GREEN "DEBUG: " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#else	//	__DEBUG__
#define	DEBUG_PRINTF( fmt, args... )
#endif	//	__DEBUG__

#ifdef	__DEBUUG_LIST__
#define	DEBUG_LIST_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)]: " GREEN "LIST: " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#else	//	__DEBUG_LIST__
#define	DEBUG_LIST_PRINTF( fmt, args... )
#endif	//	__DEBUUG_LIST__

#ifdef	__DEBUG_MSG_Q__
#define	DEBUG_MSG_Q_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)]: " GREEN "MSG_Q: " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#else	//	__DEBUG_MSG_Q__
#define	DEBUG_MSG_Q_PRINTF( fmt, args... )
#endif	//	__DEBUG_MSG_Q__

#define	INFO_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#define	INFO_BPRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( WHITE_BOLD "[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#define	INFO_YPRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( YELLOW "[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#define	INFO_YBPRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( YELLOW_BOLD "[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#define	INFO_RPRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( RED "[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#define	INFO_RBPRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( RED_BOLD"[%-16s:%5d P(%5d) T(%5ld)] INFO : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}

#define	WARN_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)]  " YELLOW_BOLD "WARN : " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}

#define	ERROR_PRINTF( fmt, args... )		\
	{ \
		time_stamp(); \
		printf( "[%-16s:%5d P(%5d) T(%5ld)] " RED_BOLD "ERROR: " fmt ENDCOLOR, __FUNCTION__, __LINE__, getpid(), gettid(), ##args ); \
	}
#endif /* ifndef __DEBUG_H__ */
