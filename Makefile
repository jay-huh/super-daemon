ifneq ($(SDK_ROOT),)
ifeq ($(CROSS_COMPILE),)
$(error : "CROSS_COMPILE Not defined!!!!")
endif
endif	#	($(SDK_ROOT),)

ifneq ($(CROSS_COMPILE),)

ifeq ($(INSTALL_DIR),)
$(error : "INSTALL_DIR Not defined!!!!")
endif

ifeq ($(SDK_ROOT),)
$(error : "SDK_ROOT( Buildroot Top Directory ) Not defined!!!!")
endif

TARGET_ROOT :=$(SDK_ROOT)/output/target

endif	#	($(CROSS_COMPILE),)

############################################################
TOP_DIR := $(CURDIR)

TARGET := daemon_main

TEST_DIR := $(TOP_DIR)/tests

COMMON_SRCS := \
	$(CURDIR)/src/mutex.c \
	$(CURDIR)/src/err.c \
	$(CURDIR)/src/debug.c

SRCS := \
	$(COMMON_SRCS) \
	$(CURDIR)/src/daemon_cdr.c \
	$(CURDIR)/src/daemonize.c \
	$(CURDIR)/src/list.c \
	$(CURDIR)/src/resource_list.c \
	$(CURDIR)/src/msg_queue.c \
	$(CURDIR)/src/sunxi.c \
	$(CURDIR)/src/reboot_file.c \
	$(CURDIR)/src/daemon_main.c

OBJS := $(SRCS:.c=.o)
OBJS := $(OBJS:.cpp=.o)

INC := \
	-I$(CURDIR)/include

LIBS := -pthread

OBJ_DIR := $(CURDIR)/obj
DEP_DIR := $(CURDIR)/dep

#DEP_FLAGS := -MT $@ -MMD -MP -MF $(DEP_DIR)/$*.d

DEPEND_FILE := $(TARGET).dep
MAP_FILE := $(TARGET).map

#CROSS_COMPILE ?= arm-linux-

CC := $(CROSS_COMPILE)gcc
CXX := $(CROSS_COMPILE)g++

CFLAGS := \
	-Wall $(INC) -g \
	-fstack-protector \
	#-O2 \
	#-fdiagnostics-color=auto

#CFLAGS += \
	-Wa,-adhlns="$@.lst"

#CFLAGS_LST = \
	-Wa,-ahlms=$@.lst

CXXFLAGS :=

LDFLAGS := \
	-Wall $(LIBS) \
	-fstack-protector \
	#-fdiagnostics-color=auto

LDFLAGS_MAP = \
	-Wl,-Map=$@.map \

LDFLAGS_LIB := \
	-shared \
	-Wl,-soname,$(TARGET_LIB)

# for x86
ifeq ($(CROSS_COMPILE),)
CFLAGS += -m32
LDFLAGS += -m32
else	#($(CROSS_COMPILE),)
# for ARM
CFLAGS += \
	-D __USE_DAEMON_SHUTDOWN__
	#-march=armv7-a -mfloat-abi=softfp -mfpu=neon \
#	-fPIE

LDFLAGS += \
	#-pie

CFLAGS += \
	-fPIC -O2 -g \
	-mapcs-frame -funwind-tables -fasynchronous-unwind-tables \
	-pipe \
	-mtune=cortex-a7 -march=armv7-a -mabi=aapcs-linux -msoft-float \
	-D_LARGEFILE_SOURCE \
	-D_LARGEFILE64_SOURCE \
	-D_FILE_OFFSET_BITS=64

CFLAGS += \
	-D_GNU_SOURCE \
	-D_REENTRANT \
	-D__OS_LINUX \
	#-D$(CDR_PLATFORM) \
	#-D$(CDR_FORMFACTOR) \
	#-D$(CDR_REVID) \
	#-D$(CDR_LANGUAGE) \
	#-D CDR_SW_VERSION=\"$(CDR_SWVER)\"

endif	#($(CROSS_COMPILE),)

CFLAGS += \
	-D KEY_BASE_FILE=\"/tmp/keyflle\"\
	-D __DEBUG_MSG_Q__ \
	#-D __DEBUG__ \
	#-D __DEBUG_LIST__ \

#CFLAGS += \
	-D __CLOSE__ALL_FDS__

# for Release: ignore assert()
# by jyhuh 2019-03-13 05:43:42
CFLAGS += \
	#-D NDEBUG

export TOP_DIR

export COMMON_SRCS COMMON_OBJS

export CC CXX
export INC LIBS CFLAGS CXXFLAGS LDFLAGS LDFLAGS_LIB
export INSTALL_DIR SDK_ROOT TARGET_ROOT

.PHONY: all clean install tests

.NOTPARALLEL: $(TARGET) tests

all: depend $(TARGET) tests

$(TARGET): $(OBJS)
	@echo "[ LD ] Linking for $@"
	@for obj in $^ ; do \
		echo "\t$$obj" ; \
	done
	@$(CXX) $(LDFLAGS) $(LDFLAGS_MAP) -o $@ $^

clean:
	@echo "[ TOP ] Cleaning..."
	@rm -f $(OBJS) $(TARGET) $(DEPEND_FILE) $(MAP_FILE) $(TARGET_TEST)
	@rm -rf $(OBJ_DIR) $(DEP_DIR) ./src/*.lst
	@$(MAKE) -C $(TEST_DIR) clean

#$(DEP_DIR)/%.d: %.c %.cpp
#	@echo "[ DEP ] Make depend file: $@ from $<"
#	$(CXX) $(INC) $(DEP_FLAGS) $<

#%.o: %.c $(DEP_DIR)/%.d
.c.o:
	@echo "[ CC ] Compiling $<"
	@$(CC) $(CFLAGS) -o $@ -c $<

#%.o: %.cpp
.cpp.o:
	@echo "[ CXX ] Compiling $<"
	@$(CXX) $(CFLAGS) $(CXXFLAGS) -o $@ -c $<

$(TARGET_TEST): $(TEST_SRCS)

tests: $(TEST_DIR)
	@echo "[ TEST ] $(TEST_DIR)"
	@$(MAKE) -C $(TEST_DIR)

depend: $(SRCS)
	@echo "============================================================"
	@echo "CROSS_COMPILE: $(CROSS_COMPILE)"
	@echo "INSTALL_DIR: $(INSTALL_DIR)"
	@echo "SDK_ROOT: $(SDK_ROOT)"
	@echo "TARGET_ROOT: $(TARGET_ROOT)"
	@echo "OBJS: $(OBJS)"
	@echo "LIBS: $(LIBS)"
	@echo "CFLAGS: $(CFLAGS)"
	@echo "CXXFLAGS: $(CXXFLAGS)"
	@echo "LDFLAGS: $(LDFLAGS)"
	@echo "LDFLAGS_LIB: $(LDFLAGS_LIB)"
	@echo "============================================================"
	@mkdir -p $(DEP_DIR)
	@echo "[ DEP ] Make depend file: $(DEPEND_FILE)"
	@$(CXX) $(INC) -MM $(SRCS) > $(DEPEND_FILE)
	@$(MAKE) -C $(TEST_DIR) depend

tags: $(SRCS)
	@echo "[ TAGS ] Make tags..."
	@mkctags.sh -r
	@mkctags.sh -c $(CURDIR)
	@echo "[ CSCOPE ] Make cscope files..."
	@mkcscope.sh -r
	@mkcscope.sh -c $(CURDIR)

install:
	@echo "============================================================"
	@echo "INSTALL_DIR: $(INSTALL_DIR)"
	@echo "SDK_ROOT: $(SDK_ROOT)"
	@echo "TARGET_ROOT: $(TARGET_ROOT)"
	@echo "[ INSTALL ] $(TARGET) ==> $(INSTALL_DIR)"
	@mkdir -p $(INSTALL_DIR)
	@cp -f $(TARGET) $(INSTALL_DIR)
	@echo "============================================================"
	#@$(MAKE) -C $(TEST_DIR) install

-include $(DEPEND_FILE)
