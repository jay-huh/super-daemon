#include	"msg_queue.h"

#include	<string.h>
#include	<stdlib.h>

//#include	<sys/types.h>
#include	<sys/ipc.h>
#include	<sys/msg.h>

#include	<fcntl.h>
#include	<errno.h>
#include	<pthread.h>

#include	"mutex.h"

#include	"debug.h"

/*	msgget
 *
 *	msgctl
 *
 *	msgsnd
 *	msgrcv
 *
 * */


static MSG_QUEUE *_pMsgQueue[ MAX_MSG_QUEUE_LIMIT ] = { NULL, };
static pthread_mutex_t		_CreateMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t		_mutex[ MAX_MSG_QUEUE_LIMIT ];

static const bool InitLock( const int idx );
static const bool DestroyLock( const int idx );
static const bool Lock( const int idx );
static const bool Unlock( const int idx );

static const bool RegisterMsgQ( const int idx, const key_t key, const int msgId );
static const int AllocMsgQ( const int idx, const key_t key );

static const int CreateMsgQ( const key_t key );
static const int GetMsgQ( const key_t key );
static const bool RemoveMsgQ( const int msgId );
static const int FindMsgQIdx( const int msgId );

static const int FindMsgQKey( const key_t key );

static const bool InitLock( const int idx )
{
	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );

	return InitMutex( &_mutex[ idx ] );
}

static const bool DestroyLock( const int idx )
{
	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );

	return DestroyMutex( &_mutex[ idx ] );
}

static const bool Lock( const int idx )
{
	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );

	return MutexLock( &_mutex[ idx ] );
}

static const bool Unlock( const int idx )
{
	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );

	return MutexUnlock( &_mutex[ idx ] );
}

const key_t GenerateKey( const char * const pKeyFile, const int projId )
{
	key_t	key = -1;
	char	keyFile[ MAX_KEY_FILE_PATH_LEN ] = DEFAULT_KEY_FILE_NAME;

	if( pKeyFile != NULL )
	{
		strncpy( keyFile, pKeyFile, MAX_KEY_FILE_PATH_LEN - 1 );
	}

	if( access( keyFile, F_OK ) < 0 )
	{
		INFO_YPRINTF( "keyFile( \"%s\" Not Found ==> Create file...\n", keyFile );
		int fd = creat( keyFile, S_IRUSR | S_IWUSR );
		if( fd < 0 )
		{
			ERROR_PRINTF( "creat(\"%s\") failed. errno: %d %s\n", keyFile, errno, strerror( errno ) );
			return -1;
		}
		close( fd );
	}

	key = ftok( keyFile, projId );
	if( key < 0 )
	{
		ERROR_PRINTF( "ftok() failed. errno: %d %s\n", errno, strerror( errno ) );
		return -1;
	}
	//INFO_YPRINTF( "KEY: file \'%s\' Magic: \'%c\' 0x%08x key: %d\n", keyFile, projId, projId, key );

	return key;
}

const bool InitMsgQueue( void )
{
	int idx = -1;

	INFO_PRINTF( "MSG_Q: size %zd * %d = %zd\n",
			sizeof( MSG_QUEUE ), MAX_MSG_QUEUE_LIMIT, sizeof( MSG_QUEUE ) * MAX_MSG_QUEUE_LIMIT );

	InitMutex( &_CreateMutex );

	MutexLock( &_CreateMutex );

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		InitLock( idx );
		Lock( idx );

		assert( _pMsgQueue[ idx ] == NULL );
		_pMsgQueue[ idx ] = NULL;

		Unlock( idx );
	}
	MutexUnlock( &_CreateMutex );

	return true;
}


const bool DeinitMsgQueue( void )
{
	int idx = -1;

	MutexLock( &_CreateMutex );

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		if( _pMsgQueue[ idx ] != NULL )
		{
			/*	TODO: close msg queue
			 *	*/
			RemoveMsgQueue( _pMsgQueue[ idx ]->id );

			DestroyLock( idx );
		}
	}

	MutexUnlock( &_CreateMutex );

	DestroyMutex( &_CreateMutex );

	return true;
}

const int CreateMsgQueue(
		const char * const pName,
		const MSG_QUEUE_TYPE type,
		const MSG_DATA_TYPE msgDataType,
		const key_t key,
		const size_t msgMaxSize )
{
	int idx = -1;
	int msgId = -1;
	char name[ MSG_QUEUE_NAME_LEN ] = DEFAULT_MSG_QUEUE_NAME;
	//MSG_QUEUE msgQueue = MSG_QUEUE_INITIALIZER;

	assert( msgMaxSize <= MSG_DATA_MAX_SIZE );
	assert( key >= 0 );

	assert( ( type >= MSG_QUEUE_TYPE_BEGIN ) && ( type < MSG_QUEUE_TYPE_END ) );
	assert( ( msgDataType >= MSG_DATA_TYPE_BEGIN ) && ( msgDataType < MSG_DATA_TYPE_END ) );

	MutexLock( &_CreateMutex );

	/*	INFO: find msg queue using the key
	 *	Update msg queue information and return...
	 *	*/
	idx = FindMsgQKey( key );
	if( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) )
	{
		Lock( idx );

		assert( key == _pMsgQueue[ idx ]->key );

		msgId = _pMsgQueue[ idx ]->id;

		_pMsgQueue[ idx ]->type = type;
		_pMsgQueue[ idx ]->msgDataType = msgDataType;
		_pMsgQueue[ idx ]->msgMaxSize = msgMaxSize;
		if( pName == NULL )
		{
			strncpy( _pMsgQueue[ idx ]->name, name, MSG_QUEUE_NAME_LEN - 1 );
		}
		else
		{
			strncpy( _pMsgQueue[ idx ]->name, pName, MSG_QUEUE_NAME_LEN - 1 );
		}

		Unlock( idx );

		INFO_BPRINTF( "MSG_Q: UPDATE: [%d] \'%s\' type: %d dataType: %d key: %d maxSize: %zd\n",
				idx, ( pName != NULL ) ? pName : name, type, msgDataType, key, msgMaxSize );

		MutexUnlock( &_CreateMutex );

		return msgId;
	}

	INFO_BPRINTF( "CREATE: \'%s\' type: %d dataType: %d key: %d maxSize: %zd\n",
			( pName != NULL ) ? pName : name, type, msgDataType, key, msgMaxSize );

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		Lock( idx );

		if( _pMsgQueue[ idx ] == NULL )
		{
			msgId = AllocMsgQ( idx, key );
			if( msgId < 0 )
			{
				ERROR_PRINTF( "MSG_Q: AllocMsgQ() failed!! errno: %d %s\n", errno, strerror( errno ) );
				break;
			}
			_pMsgQueue[ idx ]->id = msgId;
			_pMsgQueue[ idx ]->key = key;
			_pMsgQueue[ idx ]->type = type;
			_pMsgQueue[ idx ]->msgDataType = msgDataType;
			_pMsgQueue[ idx ]->msgMaxSize = msgMaxSize;
			if( pName == NULL )
			{
				strncpy( _pMsgQueue[ idx ]->name, name, MSG_QUEUE_NAME_LEN - 1 );
			}
			else
			{
				strncpy( _pMsgQueue[ idx ]->name, pName, MSG_QUEUE_NAME_LEN - 1 );
			}

			Unlock( idx );
			break;
		}
		Unlock( idx );
	}

	pthread_mutex_unlock( &_CreateMutex );

	INFO_PRINTF( "idx: %d | \'%s\' key: %d id: %d\n",
			idx, ( pName != NULL ) ? pName : name, key, msgId );

	return msgId;
}

const int GetMsgQueue( const key_t key )
{
	int idx = -1;
	int msgId = -1;

	assert( key >= 0 );

	MutexLock( &_CreateMutex );

	idx = FindMsgQKey( key );
	if( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) )
	{
		Lock( idx );

		msgId = _pMsgQueue[ idx ]->id;

		Unlock( idx );
	}
	else
	{
		msgId = GetMsgQ( key );
	}

	MutexUnlock( &_CreateMutex );

	return msgId;
}

const bool RemoveMsgQueue( const int msgId )
{
	bool ret = false;
	int idx = -1;

	assert( msgId >= 0 );

	idx = FindMsgQIdx( msgId );
	if( ( idx < 0 ) || ( idx >= MAX_MSG_QUEUE_LIMIT ) ) return false;

	Lock( idx );

	INFO_BPRINTF( "MSG_Q: REMOVE: [%d] \'%s\' key: %d msgId: %d\n",
			idx, _pMsgQueue[ idx ]->name, _pMsgQueue[ idx ]->key, _pMsgQueue[ idx ]->id );
	ret = RemoveMsgQ( msgId );
	if( ret == true )
	{
		free( _pMsgQueue[ idx ] );
		_pMsgQueue[ idx ] = NULL;
	}

	Unlock( idx );

	return ret;
}

const bool RemoveMsgQueueByKey( const key_t key )
{
	bool ret = false;
	int idx = -1;

	assert( key >= 0 );

	idx = FindMsgQKey( key );
	if( ( idx < 0 ) || ( idx >= MAX_MSG_QUEUE_LIMIT ) ) return false;

	Lock( idx );

	INFO_BPRINTF( "MSG_Q: REMOVE: [%d] \'%s\' key: %d msgId: %d\n",
			idx, _pMsgQueue[ idx ]->name, _pMsgQueue[ idx ]->key, _pMsgQueue[ idx ]->id );
	ret = RemoveMsgQ( _pMsgQueue[ idx ]->id );
	if( ret == true )
	{
		free( _pMsgQueue[ idx ] );
		_pMsgQueue[ idx ] = NULL;
	}

	Unlock( idx );

	return ret;
}

/*	NOTE: Must mutex lock / unlock outside of this function
 *	by jyhuh 2019-03-02 19:50:54
 *	*/
const bool RegisterMsgQ( const int idx, const key_t key, const int msgId )
{
	MSG_QUEUE msgQueue = MSG_QUEUE_INITIALIZER;

	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );
	assert( key >= 0 );
	assert( msgId >= 0 );
	assert( _pMsgQueue[ idx] == NULL );

	_pMsgQueue[ idx ] = malloc( sizeof( MSG_QUEUE ) );
	if( _pMsgQueue[ idx ] == NULL )
	{
		ERROR_PRINTF( "MSG_Q: malloc() failed!!\n" );
		return false;
	}

	msgQueue.id = msgId;
	msgQueue.key = key;

	memcpy( _pMsgQueue[ idx ], &msgQueue, sizeof( MSG_QUEUE ) );

	INFO_YPRINTF( "MSG_Q: REG_DFL: [%d] \'%s\' type: %d dataType: %d key: %d maxSize: %zd\n",
			idx, msgQueue.name, msgQueue.type, msgQueue.msgDataType, key, msgQueue.msgMaxSize );
	return true;
}

/*	NOTE: Must mutex lock / unlock outside of this function
 *	by jyhuh 2019-03-02 19:50:54
 *	*/
const int AllocMsgQ( const int idx, const key_t key )
{
	MSG_QUEUE msgQueue = MSG_QUEUE_INITIALIZER;

	assert( ( idx >= 0 ) && ( idx < MAX_MSG_QUEUE_LIMIT ) );
	assert( _pMsgQueue[ idx] == NULL );

	msgQueue.id = CreateMsgQ( key );
	if( msgQueue.id < 0 )
	{
		ERROR_PRINTF( "MSG_Q: CreateMsgQ() failed!! errno: %d %s\n", errno, strerror( errno ) );

		return -1;
	}

	_pMsgQueue[ idx ] = malloc( sizeof( MSG_QUEUE ) );
	if( _pMsgQueue[ idx ] == NULL )
	{
		ERROR_PRINTF( "MSG_Q: malloc() failed!!\n" );
		RemoveMsgQ( msgQueue.id );
		return -1;
	}
	msgQueue.key = key;

	memcpy( _pMsgQueue[ idx ], &msgQueue, sizeof( MSG_QUEUE ) );

	return msgQueue.id;
}

const int CreateMsgQ( const key_t key )
{
	int msgId = -1;
	int msgFlag = IPC_CREAT | S_IRUSR | S_IWUSR;

	bool removeMsgQueue = false;
	int retry = 0;

	assert( key >= 0 );

	/*	TODO: use semaphore	*/

	/*	NOTE: Create Msg Queeue
	 *	If msg queue exists, remove queue ans re-create.
	 *	by jyhuh 2019-03-02 18:09:25	*/
	do
	{
		removeMsgQueue = false;
		msgId = msgget( key, msgFlag | IPC_EXCL );
		if( msgId < 0 )
		{
			if( errno == EEXIST )
			{
				/*	Get msgid for removing Msg Queue 	*/
				msgId = msgget( key, msgFlag );
				if( msgId >= 0 )
				{
					if( RemoveMsgQ( msgId ) == false )
					{
						ERROR_PRINTF( "MSG_Q: RemoveMsgQ() failed!! errno: %d %s\n",
								errno, strerror( errno ) );
						return -1;
					}
					removeMsgQueue = true;
					retry++;
					msgId = -1;
					continue;
				}
			}
			ERROR_PRINTF( "MSG_Q: msgget() failed!! errno: %d %s\n", errno, strerror( errno ) );
			return -1;
		}
	} while( ( removeMsgQueue == true ) && ( retry < 3 ) );

	return msgId;
}

const int GetMsgQ( const key_t key )
{
	int msgId = -1;
	int msgFlag = S_IRUSR | S_IWUSR;

	bool removeMsgQueue = false;
	int retry = 0;

	int idx = -1;

	assert( key >= 0 );

	/*	TODO: use semaphore	*/

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		Lock( idx );

		if( _pMsgQueue[ idx ] == NULL )
		{
			/*	NOTE: Create Msg Queeue
			 *	If msg queue inot exists, create msg queue.
			 *	by jyhuh 2019-03-02 18:09:25	*/
			do
			{
				removeMsgQueue = false;
				//msgId = msgget( key, msgFlag | IPC_EXCL );
				msgId = msgget( key, msgFlag );
				if( msgId < 0 )
				{
					/*	INFO: msg queue that use the key not found.
					 *	*/
					if( errno == ENOENT )
					{
						msgId = CreateMsgQ( key );
#if 1
						assert( msgId >= 0 );
#else
						if( msgId < 0 )
						{
							WARN_PRINTF( "MSG_Q: CreateMsgQ() failed!! errno: %d %s\n",
									errno, strerror( errno ) );
							WARN_PRINTF( "MSG_Q: key %d retry %d\n", key, retry );
							removeMsgQueue = true;
							retry++;
							continue;
						}
#endif
						INFO_YPRINTF( "GET | No Exist => Create. key: %d msgId: %d\n", key, msgId );
						break;
					}
					ERROR_PRINTF( "MSG_Q: msgget() failed!! errno: %d %s\n", errno, strerror( errno ) );
					Unlock( idx );
					return -1;
				}
			} while( ( removeMsgQueue == true ) && ( retry < 3 ) );

			if( msgId >= 0 )
			{
				RegisterMsgQ( idx, key, msgId );
			}
			Unlock( idx );

			break;
		}

		Unlock( idx );
	}

	return msgId;
}

const bool RemoveMsgQ( const int msgId )
{
	assert( msgId >= 0 );

	/*	TODO: use semaphore	*/

	if( msgctl( msgId, IPC_RMID, NULL ) < 0 )
	{
		ERROR_PRINTF( "MSG_Q: msgctl( IPC_RMID ) failed!! errno: %d %s\n",
				errno, strerror( errno ) );
		return false;
	}
	INFO_PRINTF( "MSG_Q: REMOVE msgId: %d\n", msgId );
	return true;
}

const int FindMsgQIdx( const int msgId )
{
	int idx = -1;

	assert( msgId >= 0 );

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		Lock( idx );
		if( _pMsgQueue[ idx ] != NULL )
		{
			if( _pMsgQueue[ idx ]->id == msgId )
			{
				//DEBUG_MSG_Q_PRINTF( "Find: \'%s\' key: %d msgId: %d\n",
				//		_pMsgQueue[ idx ]->name, _pMsgQueue[ idx ]->key, _pMsgQueue[ idx ]->id );

				Unlock( idx );
				break;
			}
		}

		Unlock( idx );
	}

	if( idx >= MAX_MSG_QUEUE_LIMIT ) idx = -1;

	return idx;
}

const int FindMsgQKey( const key_t key )
{
	int idx = -1;

	assert( key >= 0 );

	for( idx = 0 ; idx < MAX_MSG_QUEUE_LIMIT ; idx++ )
	{
		Lock( idx );

		if( _pMsgQueue[ idx ] != NULL )
		{
			if( _pMsgQueue[ idx ]->key == key )
			{
				//INFO_PRINTF( "Find: \'%s\' key: %d msgId: %d\n",
				//		_pMsgQueue[ idx ]->name, _pMsgQueue[ idx ]->key, _pMsgQueue[ idx ]->id );

				Unlock( idx );
				break;
			}
		}

		Unlock( idx );
	}

	if( idx >= MAX_MSG_QUEUE_LIMIT ) idx = -1;

	return idx;
}

const bool SendMessage(
		const int msgId,
		const void * const pMsg,
		const size_t size )
{
	int idx = -1;
	int msgFlag = 0x0;

	assert( msgId >= 0 );

	idx = FindMsgQIdx( msgId );
	if( ( idx < 0 ) || ( idx >= MAX_MSG_QUEUE_LIMIT ) ) return false;

	/*	TODO: use semaphore	*/
	//	sizeof( MSG_DATA_RESOURCE ) - sizeof( long )

	if( msgsnd( msgId, pMsg, size - sizeof( long ), msgFlag ) < 0 )
	{
		ERROR_PRINTF( "MSG_Q[ %d ]: \'%s\' | msgsnd failed!! errno: %d %s\n",
				idx, _pMsgQueue[ idx ]->name, errno, strerror( errno ) );
		return false;
	}

	return true;
}

const ssize_t ReceiveMessage(
		const int msgId,
		void * const pMsg,
		const size_t size,
		const long msgType,
		const int msgFlag )
{
	int idx = -1;
	//long flag = IPC_NOWAIT | MSG_NOERROR;

	assert( msgId >= 0 );

	idx = FindMsgQIdx( msgId );
	if( ( idx < 0 ) || ( idx >= MAX_MSG_QUEUE_LIMIT ) ) return -1;

	/*	TODO: use semaphore	*/
	//	sizeof( MSG_DATA_RESOURCE ) - sizeof( long )
	/*	flag: IPC_NOWAIT, MSG_EXCEPT, MSG_NOERROR
	 *	*/
	return msgrcv( msgId, pMsg, size - sizeof( long ), msgType, msgFlag );
}
const int GetNumberOfMessages( const int msgId )
{
	int idx = -1;
	struct msqid_ds stat;

	assert( msgId >= 0 );

	idx = FindMsgQIdx( msgId );
	if( ( idx < 0 ) || ( idx >= MAX_MSG_QUEUE_LIMIT ) ) return -1;

	/*	TODO: use semaphore	*/

	if( msgctl( msgId, IPC_STAT, &stat ) < 0 )
	{
		ERROR_PRINTF( "MSG_Q: msgctl( IPC_STAT ) failed!! errno: %d %s\n",
				errno, strerror( errno ) );
		return -1;
	}

	return ( int )stat.msg_qnum;
}

