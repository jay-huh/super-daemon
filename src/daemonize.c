#include	"daemonize.h"

#include	<stdlib.h>		/* for convenience */
#include	<string.h>
#include	<fcntl.h>
#include	<signal.h>

#include	<pthread.h>

#include	<sys/stat.h>		//	umask()
#include	<sys/resource.h>	//	rtlimit

#include	<syslog.h>

#include	"err.h"

#include	"debug.h"

static bool taskRunning = false;

static void Daemonize( const char * const pCmd );
static const bool AlreadyRunning( void );
static const int Lockfile( const int fd );

const bool IsTaskRunning( void )
{
	return taskRunning;
}

void SetTaskRunning( const bool set )
{
	taskRunning = set;
	return;
}

const bool InitDaemon( DAEMON * const pDaemon, const char * const pCmd, pfnThreadFunc signalHandler )
{
	const char	*pDaemonName = NULL;

	assert( pDaemon != NULL );
	assert( pCmd != NULL );

	if( ( pDaemonName = strrchr( pCmd, '/') ) == NULL )
	{
		pDaemonName = pCmd;
	}
	else
	{
		pDaemonName++;
	}

	/*
	 * Become a daemon.
	 */
	Daemonize( pDaemonName );

	/*
	 * Make sure only one copy of the daemon is running.
	 */
	if( AlreadyRunning() == true )
	{
		WARN_PRINTF( "daemon already running\n" );
		syslog(LOG_WARNING, "daemon already running");
		exit( EXIT_FAILURE );
	}

	pDaemon->pid = getpid();
	pDaemon->tid = pthread_self();
	strncpy( pDaemon->name, pDaemonName, MAX_DAEMON_NAME_LEN - 1 );
	pDaemon->running = false;
	pDaemon->signalHandler = NULL;

	if( signalHandler != NULL )
	{
		pDaemon->signalHandler = signalHandler;
	}

	syslog( LOG_INFO, "%s: Starting Daemon. PID: %d PPID: %d", pDaemonName, getpid(), getppid() );
	INFO_BPRINTF( "%s: Starting Daemon. PID: %d PPID: %d\n", pDaemonName, getpid(), getppid() );

	return true;
}


void Daemonize( const char * const pCmd )
{
#ifdef __CLOSE__ALL_FDS__
	int i, fd0, fd1, fd2;
#endif	//	__CLOSE__ALL_FDS__
	pid_t pid, sid;
	struct rlimit rl;
	struct sigaction sa;

	/*
	 * Get maximum number of file descriptors.
	 */
	if( getrlimit( RLIMIT_NOFILE, &rl ) < 0 )
	{
		ERROR_PRINTF( "%s: can't get file limit", pCmd );
		err_quit( "%s: can't get file limit", pCmd );
	}

	/*
	 * Become a session leader to lose controlling TTY.
	 */
	pid = fork();
	if( pid  < 0 )
	{
		ERROR_PRINTF( "fork() fail.\n" );
		err_quit( "%s: can't fork", pCmd );
	}
	else if ( pid != 0 ) /* parent */
	{
		INFO_PRINTF( "parent ppid: %d pid: %d exit 0.\n", getppid(), getpid() );
		exit( EXIT_SUCCESS );
	}

	/*
	 * Clear file creation mask.
	 */
	umask( 0 );

	INFO_PRINTF( "child ppid: %d pid: %d\n", getppid(), getpid() );
	sid = setsid();
	if( sid < 0 )
	{
		ERROR_PRINTF( "setsid fail. errno: %d %s\n", errno, strerror( errno ) );
		exit( EXIT_FAILURE );
	}
	INFO_YPRINTF( "sid changed to %d. ppid: %d pid: %d\n", sid, getppid(), getpid() );

	/*
	 * Ensure future opens won't allocate controlling TTYs.
	 */
	sa.sa_handler = SIG_IGN;
	sigemptyset( &sa.sa_mask );
	sa.sa_flags = 0;
	if( sigaction( SIGHUP, &sa, NULL ) < 0 )
	{
		ERROR_PRINTF( "%s: can't ignore SIGHUP", pCmd );
		err_quit( "%s: can't ignore SIGHUP", pCmd );
	}

#if 0
	/*	NOTE: No need this fork().
	 *	by jyhuh 2019-03-05 14:58:15
	 *	*/
	pid = fork();
	if( pid  < 0 )
	{
		ERROR_PRINTF( "fork fail.\n" );
		err_quit( "%s: can't fork", pCmd );
	}
	else if (pid != 0) /* parent */
	{
		INFO_PRINTF( "parent ppid: %d pid: %d exit 0.\n", getppid(), getpid() );
		exit( EXIT_SUCCESS );
	}
#endif
	INFO_YPRINTF( "daemonize ppid: %d pid: %d\n", getppid(), getpid() );

	/*
	 * Change the current working directory to the root so
	 * we won't prevent file systems from being unmounted.
	 */
	//if( chdir( "/" ) < 0 )
	if( chdir( CH_ROOT_PATH ) < 0 )
	{
		ERROR_PRINTF( "%s: can't change directory to %s", pCmd, CH_ROOT_PATH );
		err_quit( "%s: can't change directory to /", pCmd );
	}

#ifdef __CLOSE__ALL_FDS__
	/*	NOTE: Close all open file descriptors.
	 *	Include STDIN_FILENO( 0 ), STDOUT_FILENO( 1 ), STDERR_FILENO( 2 ).
	 *	by jyhuh 2019-03-05 14:56:49
	 * */
	if( rl.rlim_max == RLIM_INFINITY )
	{
		rl.rlim_max = 1024;
	}
	for( i = 0 ; i < rl.rlim_max ; i++ )
	{
		close( i );
	}

	/*
	 * Attach file descriptors 0, 1, and 2 to /dev/null.
	 * This open() call return 0, 1, and 2.
	 * by jyhuh 2019-03-05 14:59:37
	 * */
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup( fd0 );
	fd2 = dup( fd1 );
#endif	//	__CLOSE__ALL_FDS__

	/*
	 * Initialize the log file.
	 */
	openlog( pCmd, LOG_CONS, LOG_DAEMON );
#ifdef __CLOSE__ALL_FDS__
	syslog( LOG_INFO, "Test: file descriptors 0: %d 1: %d 2: %d", fd0, fd1, fd2 );
	if( fd0 != 0 || fd1 != 1 || fd2 != 2 )
	{
		ERROR_PRINTF( LOG_ERR, "unexpected file descriptors %d %d %d", fd0, fd1, fd2 );
		syslog( LOG_ERR, "unexpected file descriptors %d %d %d", fd0, fd1, fd2 );
		exit( EXIT_FAILURE );
	}
#else
	syslog( LOG_INFO, "READY: Daemonize completed..." );
#endif	//	__CLOSE__ALL_FDS__

	return;
}

const int Lockfile( const int fd )
{
	struct flock fl;

	assert( fd >= 0 );

	fl.l_type = F_WRLCK;
	fl.l_start = 0;
	fl.l_whence = SEEK_SET;
	fl.l_len = 0;

	return( fcntl( fd, F_SETLK, &fl ) );
}

const bool AlreadyRunning( void )
{
	int     fd;
	char    buf[ 16 ];

	fd = open( DAEMON_PID_FILE, O_RDWR | O_CREAT, LOCK_MODE );
	if( fd < 0 )
	{
		ERROR_PRINTF( "open() failed... %s: errno: %d %s\n", DAEMON_PID_FILE, errno, strerror(errno) );
		syslog( LOG_ERR, "open() failed... %s: errno: %d %s", DAEMON_PID_FILE, errno, strerror(errno) );
		exit( EXIT_FAILURE );
	}
	if( Lockfile( fd ) < 0 )
	{
		if( errno == EACCES || errno == EAGAIN )
		{
			close( fd );
			return true;
		}
		syslog( LOG_ERR, "can't lock %s: %s", DAEMON_PID_FILE, strerror(errno) );
		exit( EXIT_FAILURE );
	}
	ftruncate( fd, 0 );
	sprintf( buf, "%ld", ( long )getpid() );
	write( fd, buf, strlen( buf ) + 1 );

	return false;
}

const bool SetSignalHandler( const int sigNo, pfnSignalHandler signalHandler )
{
	struct sigaction	sa;

	assert( sigNo > 0 );
	/*
	 * Restore SIGHUP default and block all signals.
	 */
	if( signalHandler == NULL )
	{
		signalHandler = SIG_DFL;
	}
	sa.sa_handler = signalHandler;
	sigemptyset( &sa.sa_mask );
	sa.sa_flags = SA_RESTART;

	if( sigaction( sigNo, &sa, NULL ) < 0 )
	{
		ERROR_PRINTF( "sigaction() failed!! sig: %d errno: %d %s\n", sigNo, errno, strerror( errno ) );
		return false;
	}
	return true;
}

const bool SetSigMask( const int sigNo )
{
	sigset_t			sigSet;
	int					err;

	assert( sigNo > 0 );

	/*	NOTE: Set Mask for using signals.
	 *	by jyhuh 2019-03-05 15:59:00
	 *	*/
	sigemptyset( &sigSet );
	sigaddset( &sigSet, sigNo );

	err = pthread_sigmask( SIG_UNBLOCK, &sigSet, NULL );
	if( err != 0 )
	{
		ERROR_PRINTF( "pthread_sigmask() failed!! sig: %d errno: %d %s\n", sigNo, err, strerror( err ) );
		return false;
	}

	return true;
}

const bool StartSignalHandler( DAEMON * const pDaemon )
{
//#ifndef	__TASK_MANAGEMENT__
	struct sigaction    sa;
//#endif	//	__TASK_MANAGEMENT__
	int			err;

	assert( pDaemon != NULL );

	if( pDaemon->signalHandler == NULL ) return false;

//#ifndef	__TASK_MANAGEMENT__
	/*
	 * Restore SIGHUP default and block all signals.
	 */
	sa.sa_handler = SIG_DFL;
	sigemptyset( &sa.sa_mask );
	sa.sa_flags = 0;

	if( sigaction( SIGHUP, &sa, NULL ) < 0 )
	{
		ERROR_PRINTF( "can't restore SIGHUP default" );
		err_quit( "can't restore SIGHUP default" );
	}
//#endif	//	__TASK_MANAGEMENT__
	/*	NOTE: Set Mask for using signals.
	 *	by jyhuh 2019-03-05 15:59:00
	 *	*/
	sigfillset( &pDaemon->sigMask );
	if( ( err = pthread_sigmask( SIG_BLOCK, &pDaemon->sigMask, NULL ) ) != 0 )
	{
		ERROR_PRINTF( "pthread_sigmask( SIG_BLOCK ) error. err: %d %s\n", err, strerror( err ) );
		err_exit( err, "SIG_BLOCK error" );
	}

	/*
	 * Create a thread to handle SIGHUP and SIGTERM.
	 */
	err = pthread_create( &pDaemon->sigTid, NULL, pDaemon->signalHandler, pDaemon );
	if( err != 0 )
	{
		ERROR_PRINTF( "can't create thread\n" );
		err_exit(err, "can't create thread");
	}

	return true;
}

const bool IsRunning( const DAEMON * const pDaemon )
{
	assert( pDaemon != NULL );

	return pDaemon->running;
}

void SetRunning( DAEMON * const pDaemon, const bool set )
{
	assert( pDaemon != NULL );

	pDaemon->running = set;

	return;
}

bool StopSignalHandler( DAEMON * const pDaemon, void * const pRetVal )
{
	int * pRet = ( int * )pRetVal;

	assert( pDaemon != NULL );

	pthread_join( pDaemon->sigTid, ( void **)pRet );

	return true;
}

