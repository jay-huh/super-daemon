#include	"list.h"

#include	<string.h>	//	memcpy
#include	<stdlib.h>	//	free

#include	"mutex.h"

#include	"debug.h"

static const bool IsEmpry( LIST * const pList );
static const bool DeleteNodeLocked( LIST * const pList, const NODE * pNode );
static NODE * const FindNodeByKeyLocked( LIST * const pList, const unsigned int key );

const bool InitList( LIST * const pList )
{
	assert( pList != NULL );

	pList->pHead = NULL;
	pList->pTail = NULL;

	pList->count = 0;

	pthread_mutex_init( &pList->mutex, NULL );

	return true;
}

const bool DeinitList( LIST * const pList )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	INFO_PRINTF( ">>> LIST: DeinitList %p cnt: %ld\n", pList, pList->count );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		DeleteNodeLocked( pList, pIter );
		pIter = pList->pHead;
	}

	MutexUnlock( &pList->mutex );

	INFO_PRINTF( ">>> LIST: DeinitList %p cnt: %ld\n", pList, pList->count );

	pthread_mutex_destroy( &pList->mutex );

	return true;
}

const bool IsEmpry( LIST * const pList )
{
	bool res = false;

	assert( pList != NULL );

	if( ( pList->pHead == NULL ) && ( pList->pTail == NULL ) )
	{
		res = true;
	}

	return res;
}

const bool IsListEmpry( LIST * const pList )
{
	bool res = false;

	assert( pList != NULL );

	MutexLock( &pList->mutex );

	res = IsEmpry( pList );

	MutexUnlock( &pList->mutex );

	return res;
}

const bool AddNode( LIST * const pList, NODE * const pNode )
{
	NODE	*pNewNode = NULL;

	assert( pList != NULL );
	assert( pNode != NULL );

	MutexLock( &pList->mutex );

	if( FindNodeByKeyLocked( pList, pNode->key ) != NULL )
	{
		ERROR_PRINTF( "LIST: key %d was used in List!!\n", pNode->key );
		MutexUnlock( &pList->mutex );
		return false;
	}

	pNewNode = malloc( sizeof( NODE ) );
	if( pNewNode == NULL )
	{
		ERROR_PRINTF( "LIST: malloc() failed!!\n" );
		MutexUnlock( &pList->mutex );
		return false;
	}
	memcpy( pNewNode, pNode, sizeof( NODE ) );

	if( IsEmpry( pList ) == true )
	{
		pList->pHead = pNewNode;
		pList->pTail = pNewNode;

		pList->pTail->pNext = NULL;
	}
	else
	{
		assert( pList->pTail->pNext == NULL );

		pList->pTail->pNext = pNewNode;
		pList->pTail = pNewNode;
	}
	pList->count++;

	DEBUG_LIST_PRINTF( "ADD: H %p T %p node %ld: %p next: %p\n",
			pList->pHead, pList->pTail, pNode->resource.seq,
			pNode, pNode->pNext );

	MutexUnlock( &pList->mutex );

	return true;
}

const bool DeleteNode( LIST * const pList, const NODE * pNode )
{
	bool res = false;
	NODE *pIter = NULL;

	assert( pList != NULL );

	if( pNode == NULL ) return false;

	MutexLock( &pList->mutex );

	res =  DeleteNodeLocked( pList, pIter );

	MutexUnlock( &pList->mutex );

	return res;
}

const bool DeleteNodeLocked( LIST * const pList, const NODE * pNode )
{
	bool res = false;
	NODE *pIter = NULL;

	assert( pList != NULL );

	if( pNode == NULL ) return false;

	if( IsEmpry( pList ) == true )
	{
		return false;
	}

	pIter = pList->pHead;

	if( pIter == pNode )
	{
		pList->pHead = pList->pHead->pNext;
		if( pNode == pList->pTail )
		{
			pList->pTail = NULL;
		}
		DEBUG_LIST_PRINTF( "REMOVE_H node %ld: %p next: %p\n", pIter->resource.seq, pIter, pIter->pNext );
		free( ( void * )pNode);

		res = true;
	}
	else
	{
		while( ( pIter != NULL ) && ( pIter->pNext != NULL ) )
		{
			if( pIter->pNext == pNode )
			{
				DEBUG_LIST_PRINTF( "REMOVE next -> node %ld: %p next->next: %p\n", pIter->pNext->resource.seq, pIter->pNext, pIter->pNext->pNext );
				if( pIter->pNext == pList->pTail )
				{
					pList->pTail = pIter;
				}
				pIter->pNext = pIter->pNext->pNext;

				free( ( void * )pNode);

				res = true;
				break;
			}
			pIter = pIter->pNext;
		}
	}

	if( res == true )
	{
		pList->count--;
	}

	return res;
}

const bool UpdateNode( LIST * const pList, const NODE * const pNode )
{
	NODE *pUpdateNode = NULL;

	assert( pList != NULL );
	assert( pNode != NULL );

	MutexLock( &pList->mutex );

	pUpdateNode = FindNodeByKeyLocked( pList, pNode->key );

	if( pUpdateNode == NULL )
	{
		MutexUnlock( &pList->mutex );
		return false;
	}

	memcpy( pUpdateNode->data, pNode->data, NODE_DATA_SIZE );

	MutexUnlock( &pList->mutex );

	return true;
}

NODE * const FindNode( LIST * const pList, const NODE * const pNode )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( pNode != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter == pNode )
		{
			break;
		}
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const FindNodeByKeyLocked( LIST * const pList, const unsigned int key )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter->key == key )
		{
			break;
		}
		pIter = pIter->pNext;
	}

	return pIter;
}

NODE * const FindNodeByKey( LIST * const pList, const unsigned int key )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	MutexLock( &pList->mutex );

	pIter = FindNodeByKeyLocked( pList, key );

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const IterBegin( LIST * const pList )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const IterEnd( LIST * const pList )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pTail;

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const IterNext( LIST * const pList, const NODE * const pNode )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( pNode != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter == pNode )
		{
			//INFO_PRINTF( "LIST: %p cnt: %ld node %ld: %p next: %p\n", pList, pList->count,
			//		pIter->resource.seq, pIter, pIter->pNext );
			break;
		}
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return pIter->pNext;
}

const int GetNodeCount( const LIST * const pList )
{
	assert( pList != NULL );

	return pList->count;
}

const bool PrintlList( LIST * const pList )
{
	NODE *pIter = NULL;

	assert( pList != NULL );

	MutexLock( &pList->mutex );

	INFO_PRINTF( ">>> LIST: %p cnt: %ld\n", pList, pList->count );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		INFO_PRINTF( "LIST: %p cnt: %ld node: %p key: %d next: %p\n", pList, pList->count,
				pIter, pIter->key, pIter->pNext );
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return true;
}

