#include	"mutex.h"

#include	<string.h>
#include	<errno.h>

#include	"debug.h"

const bool InitMutex( pthread_mutex_t * const pMutex )
{
	pthread_mutexattr_t attr;

	assert( pMutex != NULL );

	pthread_mutexattr_init( &attr );

	pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_ERRORCHECK );

	if( pthread_mutex_init( pMutex, &attr ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: InitMutex() failed. errno: %d %s\n", errno, strerror( errno ) );
		pthread_mutexattr_destroy( &attr );
		return false;
	}
	pthread_mutexattr_destroy( &attr );

	return true;
}

const bool DestroyMutex( pthread_mutex_t * const pMutex )
{
	assert( pMutex != NULL );

	if( pthread_mutex_destroy( pMutex ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: DestroyMutex() failed. errno: %d %s\n", errno, strerror( errno ) );
		return false;
	}
	return true;
}

const bool MutexLock( pthread_mutex_t * const pMutex )
{
	assert( pMutex != NULL );

	if( pthread_mutex_lock( pMutex ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: MutexLock() failed. errno: %d %s\n", errno, strerror( errno ) );
		return false;
	}
	return true;
}

const bool MutexTryLock( pthread_mutex_t * const pMutex )
{
	assert( pMutex != NULL );

	if( pthread_mutex_trylock( pMutex ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: MutexLock() failed. errno: %d %s\n", errno, strerror( errno ) );
		return false;
	}
	return true;
}

const bool MutexTimedLock( pthread_mutex_t * const pMutex, const int timeout )
{
	struct timespec ts;

	assert( pMutex != NULL );

	ts.tv_sec = timeout / 1000;
	ts.tv_nsec = ( timeout % 1000 ) * 1e9;

	if( pthread_mutex_timedlock( pMutex, &ts ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: MutexLock() failed. errno: %d %s\n", errno, strerror( errno ) );
		return false;
	}
	return true;
}

const bool MutexUnlock( pthread_mutex_t * const pMutex )
{
	assert( pMutex != NULL );

	if( pthread_mutex_unlock( pMutex ) < 0 )
	{
		ERROR_PRINTF( "MUTEX: MutexLock() failed. errno: %d %s\n", errno, strerror( errno ) );
		return false;
	}
	return true;
}
