#include	"daemon_cdr.h"

#include	<stdlib.h>	//	exit
#include	<string.h>	//	memset

#include	<unistd.h>	//	execv

#include	<syslog.h>
#include	<signal.h>

#include	"debug.h"
#include	<errno.h>

const bool RunTask( void )
{
	//#define	CDR_FULL_PATH	"/opt/daemon/msg_test_sender"
	//#define	CDR_FILE_NAME	"msg_test_sender"

#define	CDR_FULL_PATH	"/home/jyhuh/work/m3_platform/buildroot-2015.05/3rd_party/mbrain/v3/sources/daemon/tests/msg_queue_sender"
#define	CDR_FILE_NAME	"msg_queue_sender"
	pid_t pid;
	char *pTaskArgv[ 128 ] = { CDR_FULL_PATH, 0 };
	char task[ 128 ] = CDR_FULL_PATH;

	pid = fork();
	if( pid == 0 )	//	Child
	{
		INFO_BPRINTF( ">>>>> DAEMON: Start Child \'%s\'ppid: %d pid: %d\n", CDR_FULL_PATH, getppid(), getpid() );
		/*	NOTE: Need for copy resource and context.
		 *	by jyhuh 2019-03-08 00:19:44
		 *	*/
		sleep( 1 );

		if( execv( task, pTaskArgv ) < 0 )
		//if( execl( task, task, NULL ) < 0 )
		{
			ERROR_PRINTF( "execl() fail. errno: %d %s\n", errno, strerror( errno ) );
		}
		//INFO_BPRINTF( ">>>>> DAEMON: Exit Child \'%s\'ppid: %d pid: %d\n", CDR_FULL_PATH, getppid(), getpid() );
		exit( 0 );
	}
	else if( pid  > 0 )		//	Parent ==> Daemon
	{
		INFO_BPRINTF( " >>>>>> DAEMON: parent ppid: %d pid: %d child pid: %d\n", getppid(), getpid(), pid );
		SetTaskRunning( true );
		//RegisterTaskPid( pid );
	}
	else
	{
		ERROR_PRINTF( "fork() fail. errno: %d %s\n", errno, strerror( errno ) );
		exit( EXIT_FAILURE );
	}
	return true;
}

int main( int argc, char *argv[] )
{
	DAEMON_CDR		*pDaemon = NULL;
	SYSTEM_STATUS	status = SYSTEM_STATUS_HEALTHY;

	pDaemon = InitDaemonCdr( argv[ 0 ] );
	if( pDaemon == NULL )
	{
		ERROR_PRINTF( "InitDaemon() failed!!!\n" );
		exit( EXIT_FAILURE );
	}

	SetSystemStatus( SYSTEM_STATUS_HEALTHY );

	SetRunning( ( DAEMON * const )pDaemon, true );

	StartSignalHandler( ( DAEMON * const )pDaemon );

	StartReceiver( pDaemon->msgTid );

	/*	NOTE: Need for copy resource and context.
	 *	by jyhuh 2019-03-08 00:19:44
	 *	*/
	//usleep( 10 * 1000 );

	int count = 0;

	SetSigMask( SIGUSR1 );

	DEBUG_PRINTF( "START LOOP\n" );
	while( IsRunning( ( DAEMON * const )pDaemon ) == true )
	{
		if( count < 10 )
		{
			//usleep( 100 * 1000 );
			if( usleep( 100 * 1000 ) < 0 )
			{
				WARN_PRINTF("errno: %d %s\n", errno, strerror( errno ) );
			}
			count++;
			continue;
		}
		count = 0;

#ifdef	__TASK_MANAGEMENT__
		if( IsTaskRunning() == false )
		{
			RunTask();
		}
#endif	//	__TASK_MANAGEMENT__
	}
	DEBUG_PRINTF( "EXIT LOOP\n" );

	StopReceiver( pDaemon->msgTid );

	StopSignalHandler( ( DAEMON * const )pDaemon, NULL );

	DEBUG_PRINTF( "join Signal Thread\n" );

	status = DeinitDaemonCdr();
	INFO_BPRINTF( "LAST SYSTEM_STATUS: %d\n", status );

	switch( status )
	{
		case SYSTEM_STATUS_HEALTHY :
			INFO_YBPRINTF( "status %d => SYSTEM_STATUS_HEALTHY\n", status );
			break;
		case SYSTEM_STATUS_REBOOT :
			INFO_YBPRINTF( "status %d => SYSTEM_STATUS_REBOOT\n", status );
			RebootSystem();
			break;
		case SYSTEM_STATUS_POWER_DOWN :
			INFO_YBPRINTF( "status %d => SYSTEM_STATUS_POWER_DOWN\n", status );
			ShutdownSystem();
			break;
		case SYSTEM_STATUS_FORCE_POWER_DOWN :
			INFO_YBPRINTF( "status %d => SYSTEM_STATUS_FORCE_POWER_DOWN\n", status );
			ShutdownSystem();
			break;
		default:
			break;
	}

	syslog( LOG_INFO, "Gracefully shutdown." );
	INFO_YBPRINTF( "Gracefully shutdown.\n" );

	exit( EXIT_SUCCESS );
}
