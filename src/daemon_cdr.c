#include	"daemon_cdr.h"

#include	<stdlib.h>	//	exit
#include	<string.h>	//	strerror
#include	<signal.h>
#include	<errno.h>

#include	<wait.h>
#include	<syslog.h>
#include	<sys/msg.h>

#include	<sys/reboot.h>
#include	<linux/reboot.h>

#include	"msg_queue.h"
#include	"mutex.h"

#include	"sunxi.h"
#include	"reboot_file.h"

#include	"debug.h"

static DAEMON_CDR			*s_pDaemon = NULL;
static pthread_mutex_t		_Mutex = PTHREAD_MUTEX_INITIALIZER;
/*	INFO: Need for Reboot or Shutdown processing.	*/
static volatile SYSTEM_STATUS		s_lastStatus = SYSTEM_STATUS_HEALTHY;

static LIST * const GetList( const RESOURCE_TYPE type );

static const bool HealthCheck( void );
static void SignalHandler( int sigNo );

DAEMON_CDR * const InitDaemonCdr( const char * const pCmd )
{
	int idx;

	assert( pCmd != NULL );
	assert( s_pDaemon == NULL );

	GetRebootStatusFile();

	s_pDaemon = malloc( sizeof( DAEMON_CDR ) );
	if( s_pDaemon == NULL )
	{
		ERROR_PRINTF( "DAEMON: malloc() failed!!\n" );
		return NULL;
	}

	memset( s_pDaemon, 0x0, sizeof( DAEMON_CDR ) );

	InitMutex( &_Mutex );

	InitList( &s_pDaemon->processList );
	InitList( &s_pDaemon->threadList );

	if( InitDaemon( ( DAEMON * const )s_pDaemon, pCmd, ThreadSignalHandler ) == false )
	{
		ERROR_PRINTF( "InitDaemon() failed!!!\n" );
		exit( EXIT_FAILURE );
	}
	SetSystemStatus( SYSTEM_STATUS_HEALTHY );

	for( idx = 0 ; idx < MAX_TASK ; idx++ )
	{
		s_pDaemon->taskPid[ idx ] = -1;
	}

	return s_pDaemon;
}

const SYSTEM_STATUS DeinitDaemonCdr( void )
{
	const SYSTEM_STATUS		status = GetSystemStatus();

	assert( s_pDaemon != NULL );

	//PrintlList( &s_pDaemon->processList );
	DeinitList( &s_pDaemon->processList );

	//PrintlList( &s_pDaemon->threadList );
	DeinitList( &s_pDaemon->threadList );

	free( s_pDaemon );
	s_pDaemon = NULL;

	DestroyMutex( &_Mutex );

	/*
	 * Proceed with the rest of the daemon.
	 */
	/* ... */
	if( unlink( DAEMON_PID_FILE ) < 0 )
	{
		ERROR_PRINTF( "unlink() failed... errno: %d %s\n", errno, strerror( errno ) );
	}

	return status;
}

DAEMON_CDR * const GetDaemonCdr( void )
{
	return s_pDaemon;
}

void SetSystemStatus( const SYSTEM_STATUS status )
{
	assert( s_pDaemon != NULL );

	MutexLock( &_Mutex );

	if( status != SYSTEM_STATUS_HEALTHY )
	{
		DEBUG_PRINTF( "status: %d => %d\n", s_pDaemon->systemStatus, status );
	}

	s_pDaemon->systemStatus = status;
	s_lastStatus = status;

	MutexUnlock( &_Mutex );

	return;
}

const SYSTEM_STATUS GetSystemStatus( void )
{
	SYSTEM_STATUS	status;

	assert( s_pDaemon != NULL );

	MutexLock( &_Mutex );

	status = s_pDaemon->systemStatus;

	MutexUnlock( &_Mutex );

	return status;
}

const bool StartReceiver( pthread_t tid[] )
{
	int		err = -1;
	int		i;

	assert( tid != NULL );
	assert( s_pDaemon != NULL );

	MutexLock( &_Mutex );

	InitMsgQueue();

	InitPmic();
	InitWatchdog();

	syslog( LOG_INFO, "Init. Msg. Queue." );
	INFO_PRINTF( "Init. Msg. Queue.\n" );

	SetSignalHandler( SIGUSR1, SignalHandler );

	for( i = 0 ; i < MAX_RECEIVER ; i++ )
	{
		err = pthread_create( &tid[ i ], NULL, ThreadReceiver, s_pDaemon );
		if( err != 0 )
		{
			ERROR_PRINTF( "pthread_create() failed... errno: %d %s\n", err, strerror( err ) );
		}
	}
	err = pthread_create( &s_pDaemon->aliveTid, NULL, ThreadAlive, s_pDaemon );
	if( err != 0 )
	{
		ERROR_PRINTF( "pthread_create() failed... errno: %d %s\n", err, strerror( err ) );
	}

	MutexUnlock( &_Mutex );

	return true;
}

const bool StopReceiver( pthread_t tid[] )
{
	int		i;

	assert( s_pDaemon != NULL );

	for( i = 0 ; i < MAX_RECEIVER ; i++ )
	{
		pthread_join( tid[ i ], NULL );
	}

	pthread_join( s_pDaemon->aliveTid, NULL );

	MutexLock( &_Mutex );

	DeinitMsgQueue();


	/*	NOTE: Do NOT Disable Watchdog for abnormal conditions.
	 *	by jyhuh 2019-03-11 22:34:54
	 *	*/
	//ClosePmic();
	//CloseWatchdog();

	MutexUnlock( &_Mutex );

	return true;
}

void * ThreadAlive( void *pArg )
{
	DAEMON_CDR * const	pDaemonCdr = ( DAEMON_CDR * const )( pArg );
	//MSG_DATA_RESOURCE	msgDataResource;

	int count = 0;
	int ret = 0;
	bool poweroff = false;
	int healthCount = 0;

	assert( pDaemonCdr != NULL );

	SetSigMask( SIGUSR1 );

	syslog( LOG_INFO, "Start Alive Thread.\n" );
	INFO_BPRINTF( "Start Alive Thread.\n" );

	while( IsRunning( ( DAEMON * const )s_pDaemon ) == true )
	{
		if( count < 10 )
		{
			//usleep( 100 * 1000 );
			if( usleep( 100 * 1000 ) < 0 )
			{
				WARN_PRINTF("errno: %d %s\n", errno, strerror( errno ) );
			}
			count++;
			continue;
		}
		count = 0;

		/*	NOTE: Watchdog Reboot Count clear condition
		 *	1. ACC IN
		 *	2. continuously healthy condition over 30 seconds.
		 *	*/
		if(  GetRebootCount( REBOOT_BY_WATCHDOG ) > 0 )
		{
			if( IsAccValid() == true )
			{
				UpdateRebootStatusFile( REBOOT_BY_ALL, REBOOT_REASON_CMD_CLEAR );
			}
		}

		if( HealthCheck() == false )
		{
			pDaemonCdr->alertCount++;
			healthCount = 0;
			WARN_PRINTF( "HEALTH: alertCount: %d\n", pDaemonCdr->alertCount );
			if( pDaemonCdr->alertCount > TIME_SEC_EXPECT_WDT )
			{
				/*	TODO: Update log file
				 *	- Expected Reboot by Watchdog
				 *	by jyhuh 2019-03-08 14:19:26
				 *	*/
				if( IsUpdatedRebootStatusFile() == false )
				{
					poweroff = UpdateRebootStatusFile( REBOOT_BY_WATCHDOG, REBOOT_REASON_CMD_INCREASE );
#ifndef	__USE_DAEMON_SHUTDOWN__
					SetSystemStatus( SYSTEM_STATUS_REBOOT );
#endif	//	__USE_DAEMON_SHUTDOWN__
				}
#if 1
				ERROR_PRINTF( "HEALTH %d: Maybe Reboot by WDT in a few seconds. poweroff: %d\n",
						pDaemonCdr->alertCount, poweroff );
#else
				int leftTime = GetWatchdogLeftTime();
				ERROR_PRINTF( "HEALTH %d: Maybe Reboot by WDT in a few seconds: %d. poweroff: %d\n",
						pDaemonCdr->alertCount, leftTime, poweroff );
#endif

			}

			if( poweroff == true )
			{
				SetSystemStatus( SYSTEM_STATUS_FORCE_POWER_DOWN );
				//SetRunning( ( DAEMON * const )&( pDaemonCdr->daemon ), false );
				//pthread_kill( pDaemonCdr->daemon.sigTid, SIGUSR1 );
			}
		}
		else
		{
			KeepAliveWatchdog();
			pDaemonCdr->alertCount = 0;
			healthCount++;

			if(  GetRebootCount( REBOOT_BY_WATCHDOG ) > 0 )
			{
				/*	NOTE: Watchdog Reboot Count clear condition
				 *	1. ACC IN
				 *	2. continuously healthy condition over 30 seconds.
				 *	*/
				if( ( IsAccValid() == true ) || ( healthCount >= TIME_SEC_CLEAR_REBOOT_COUNT ) )
				{
					UpdateRebootStatusFile( REBOOT_BY_ALL, REBOOT_REASON_CMD_CLEAR );
				}
			}
		}
	}

	INFO_BPRINTF( "Exit Alive Thread.\n" );

	pthread_exit( ( void * )&ret );
}

void * ThreadReceiver( void *pArg )
{
	DAEMON_CDR * const	pDaemonCdr = ( DAEMON_CDR * const )( pArg );
	MSG_DATA_RESOURCE	msgDataResource;

	int		msgId = -1;
	key_t	key = -1;

	ssize_t				len;
	int					messages = -1;
	//int i = 0;
	int ret = 0;

	assert( pDaemonCdr != NULL );

	SetSigMask( SIGUSR1 );

	key = GenerateKey( NULL, MSG_QUEUE_MAGIC_KEY );
	if( key < 0 )
	{
		ERROR_PRINTF( "GenerateKey() failed. errno: %d %s\n", errno, strerror( errno ) );
		return 0;
	}

	msgId = CreateMsgQueue( "MSG_Q_RESOURCE", MSG_QUEUE_TYPE_SYSV, MSG_DATA_TYPE_RESOURCE, key, 512 );

	syslog( LOG_INFO, "Start Receiver Thread. msgId: %d( 0x%08x )", msgId, msgId );
	INFO_BPRINTF( "Start Receiver Thread. msgId: %d( 0x%08x )\n", msgId, msgId );

	while( IsRunning( ( DAEMON * const )s_pDaemon ) == true )
	{
		/*	TODO:
		 *	1. check status of processes.
		 *	2. check msg queue.
		 *
		 *	by jyhuh 2019-03-05 18:19:36
		 *	*/
		if( GetSystemStatus() != SYSTEM_STATUS_HEALTHY )
		{
			SetRunning( ( DAEMON * const )&( pDaemonCdr->daemon ), false );
			pthread_kill( pDaemonCdr->daemon.tid, SIGUSR1 );
			pthread_kill( pDaemonCdr->daemon.sigTid, SIGUSR1 );
			pthread_kill( pDaemonCdr->aliveTid, SIGUSR1 );
			break;
		}

		//usleep( 10 * 1000 );
#if 1
		len = ReceiveMessage( msgId, &msgDataResource, sizeof( MSG_DATA_RESOURCE ), 0, MSG_NOERROR );
		if( len < 0 )
		{
			ERROR_PRINTF( "msgrcv() failed. errno: %d %s\n", errno, strerror( errno ) );
			continue;
		}
		ParseMessage( &msgDataResource );

#else
		messages = GetNumberOfMessages( msgId );
		if( messages <= 0 )
		{
			//usleep( 100 * 1000 );
			if( usleep( 100 * 1000 ) < 0 )
			{
				WARN_PRINTF("errno: %d %s\n", errno, strerror( errno ) );
			}
			continue;
		}

		for( i = 0 ; i < messages ; i++ )
		{
			len = ReceiveMessage( msgId, &msgDataResource,
					sizeof( MSG_DATA_RESOURCE ), 0, IPC_NOWAIT | MSG_NOERROR );
			if( len < 0 )
			{
				//ERROR_PRINTF( "msgrcv() failed. errno: %d %s\n", errno, strerror( errno ) );
				continue;
			}
			ParseMessage( &msgDataResource );

#if 0
			count++;
			if( ( count % 1000 ) == 0 )
			{
				INFO_YPRINTF( "RECV: \'%s\' msgId: %d | seq: %ld i: %d\n",
						msgDataResource.resource.name, msgId, msgDataResource.resource.seq, i );
			}
#endif
		}
#endif
	}
	messages = GetNumberOfMessages( msgId );
	WARN_PRINTF( "RECV: LAST messages: %d\n", messages );

	pthread_exit( ( void * )&ret );
}

const bool HealthCheck( void )
{
	LIST	*pList = NULL;
	NODE	*pIter = NULL;
	bool	healthy = true;
	int		idx = 0;

	pList = GetList( RESOURCE_TYPE_THREAD );

	MutexLock( &_Mutex );

	//for( pIter = IterBegin( pList ) ; pIter != IterEnd( pList ) ; pIter = IterNext( pList, pIter ) )
	for( pIter = IterBegin( pList ) ; pIter != NULL ; pIter = IterNext( pList, pIter ) )
	{
		RESOURCE * const pResource = &pIter->resource;

		//INFO_YPRINTF( "%2d Iter: %p pNext: %p count: %d\n", i++, pIter, IterNext( pList, pIter ), GetNodeCount( pList ) );
		if( pResource->status == RESOURCE_STATUS_WAIT_HEARTBEAT )
		{
			if( pResource->isAlive == false )
			{
				healthy = false;

				WARN_PRINTF( "[%-16s] %2d/%2d BAD tid: %d seq %ld | status %d / command %d\n",
						pResource->name,
						idx, GetNodeCount( pList ),
						pResource->tid,
						pResource->seq,
						pResource->status,
						pResource->command
						);
				break;
			}
		}
		idx++;
	}

	/*	NOTE: All resources are healthy. then clear alive flag.
	 *	by jyhuh 2019-03-10 13:20:18
	 *	*/
	if( healthy == true )
	{
		for( pIter = IterBegin( pList ) ; pIter != NULL ; pIter = IterNext( pList, pIter ) )
		{
			RESOURCE * const pResource = &pIter->resource;

			pResource->isAlive = false;
		}
	}

	MutexUnlock( &_Mutex );

	return healthy;;
}

void reread( void )
{
	/* ... */
}

void SignalHandler( int sigNo )
{
	const char *pSigName = NULL;

	switch( sigNo )
	{
		case SIGCHLD:
			pSigName = "SIGCHLD";
			break;
		case SIGHUP:
			pSigName = "SIGHUP";
			break;
		case SIGTERM:
			pSigName = "SIGTERM";
			break;
		case SIGUSR1:
			pSigName = "SIGUSR1";
			break;
		default:
			pSigName = "Not Hanled";
			break;
	}
	INFO_RBPRINTF( "SIG %d: \'%s\' status: %d...\n", sigNo, pSigName, GetSystemStatus() );
	return;
}

void SignalHandlerSigchld( const int option )
{
	/* ... */
	pid_t	pid = -1;
	int		status;

	//pid = waitpid( -1, &status, WNOHANG );
	pid = waitpid( 0, &status, option );
	/*	NOTE: 1st arg: 0 => same GID child.
	 *	by jyhuh 2019-03-08 09:59:30
	 *	*/
	if( pid < 0 )
	{
		ERROR_PRINTF( "waitpid() failed... errno: %d %s\n", errno, strerror( errno ) );
	}
	else if( pid > 0 )
	{
		DeregisterTaskPid( pid );
	}

	SetTaskRunning( false );

	INFO_RBPRINTF( "SIGCHLD: pid: %d status: %d\n", pid, status );

	return;
}

void * ThreadSignalHandler( void *pArg )
{
	DAEMON_CDR * const	pDaemonCdr = ( DAEMON_CDR * const )( pArg );
	sigset_t	*pSigSet = NULL;
	int err, signo;
	int ret = 0;

	assert( pDaemonCdr != NULL );

	pSigSet = &( pDaemonCdr->daemon.sigMask );

	INFO_BPRINTF( "Thread: ppid: %d pid: %d\n", getppid(), getpid() );

	/*	TODO: Apply signalfd with select() or poll().
	 *	by jyhuh 2019-03-05 15:59:47
	 *	*/
	while( IsRunning( ( DAEMON * const )&( pDaemonCdr->daemon ) ) == true )
	{
		err = sigwait( pSigSet, &signo );
		if( err != 0 )
		{
			ERROR_PRINTF( "sigwait() failed... errno: %d %s\n", errno, strerror( errno ) );
			syslog( LOG_ERR, "sigwait failed" );
			if( unlink( DAEMON_PID_FILE ) < 0 )
			{
				ERROR_PRINTF( "unlink() failed... errno: %d %s\n", errno, strerror( errno ) );
			}
			exit( EXIT_FAILURE );
		}

		switch( signo )
		{
			case SIGCHLD:
				INFO_RBPRINTF( "SIG %d: SIGCHLD| Wait termination.", signo );
				syslog( LOG_INFO, "SIGCHLD: Wait termination." );
				SignalHandlerSigchld( WNOHANG );
				break;

			case SIGHUP:
				INFO_RBPRINTF( "SIG %d: SIGHUP | Re-reading configuration file\n", signo );
				syslog( LOG_INFO, "Re-reading configuration file" );
				reread();
				break;
			case SIGTERM:
				INFO_RBPRINTF( "SIG %d: SIGTERM | Terminate...\n", signo );
				syslog( LOG_INFO, "got SIGTERM exiting" );
				SetRunning( ( DAEMON * const )&( pDaemonCdr->daemon ), false );
				break;
			case SIGUSR1:
				INFO_RBPRINTF( "SIG %d: SIGUSR1 | Terminate by command =>status: %d...\n",
						signo, GetSystemStatus() );
				break;
			default:
				INFO_RBPRINTF( "SIG %d: ...\n", signo );
				syslog( LOG_INFO, "unexpected signal %d\n", signo );
				break;
		}
	}

	if( IsTaskRunning() == true )
	{
		/*	TODO: Send sigal or message to the task for termination.
		 *	by jyhuh 2019-03-08 10:12:01
		 *	*/
		SignalHandlerSigchld( 0 );
	}

	INFO_YPRINTF( "SIG: Exit Signal Thread...\n" );

	pthread_exit( ( void * )&ret );
}

LIST * const GetList( const RESOURCE_TYPE type )
{
	assert( s_pDaemon !=NULL );
	assert( ( type >= RESOURCE_TYPE_PROCESS ) && ( type < RESOURCE_TYPE_END ) );

	switch( type )
	{
		case RESOURCE_TYPE_PROCESS : return  &s_pDaemon->processList;
			break;
		case RESOURCE_TYPE_THREAD : return &s_pDaemon->threadList;
			break;
		default : return &s_pDaemon->threadList;
			break;
	}
	return NULL;
}

const bool RegisterResource( const RESOURCE * const pResource )
{
	LIST	*pList = NULL;
	NODE	node = RESOURCE_NODE_INITIALIZER;

	assert( pResource !=NULL );

	pList = GetList( pResource->type );

	node.pNext = NULL;
	node.key = pResource->tid;

	memcpy( &node.resource, pResource, sizeof( RESOURCE ) );

	node.resource.status = RESOURCE_STATUS_WAIT_HEARTBEAT;
	//node.resource.command = SYSTEM_CMD_NONE;
	node.resource.isAlive = true;

	MutexLock( &_Mutex );

	if( AddNode( pList, &node ) == false )
	{
		ERROR_PRINTF( "DAEMON_CDR: AddNode() failed!!\n" );
		MutexUnlock( &_Mutex );
		return false;
	}

	MutexUnlock( &_Mutex );

	INFO_PRINTF( "%d: REGISTER: %s pid: %d tid: %d\n", GetNodeCount( pList ), pResource->name, pResource->pid, pResource->tid );

	return true;
}

const bool SleepResource( const RESOURCE * const pResource )
{
	LIST	*pList = NULL;
	NODE	node = RESOURCE_NODE_INITIALIZER;

	assert( pResource !=NULL );

	pList = GetList( pResource->type );

	node.pNext = NULL;
	node.key = pResource->tid;

	memcpy( &node.resource, pResource, sizeof( RESOURCE ) );

	node.resource.status = RESOURCE_STATUS_SLEEP;
	node.resource.command = SYSTEM_CMD_NONE;
	node.resource.isAlive = true;

	MutexLock( &_Mutex );

	if( UpdateNode( pList, &node ) == false )
	{
		ERROR_PRINTF( "DAEMON_CDR: UpdateNode() failed!!\n" );
		MutexUnlock( &_Mutex );
		return false;
	}

	MutexUnlock( &_Mutex );

	//INFO_PRINTF( "%d: SLEEP: %s pid: %d tid: %d\n", GetNodeCount( pList ), pResource->name, pResource->pid, pResource->tid );

	return true;
}

const bool AliveResource( const RESOURCE * const pResource )
{
	LIST	*pList = NULL;
	NODE	node = RESOURCE_NODE_INITIALIZER;
	SYSTEM_COMMAND	command;
	RESOURCE_STATUS	status;

	assert( pResource !=NULL );

	pList = GetList( pResource->type );

	node.pNext = NULL;
	node.key = pResource->tid;

	memcpy( &node.resource, pResource, sizeof( RESOURCE ) );

	command =  pResource->command;
	status =  pResource->status;

	if( ( status == RESOURCE_STATUS_COMMAND ) && ( command != SYSTEM_CMD_NONE ) )
	{
		switch( command )
		{
			case SYSTEM_CMD_NONE :
				INFO_YPRINTF( "CMD: status %d => ommand %d => SYSTEM_CMD_NONE\n", GetSystemStatus(), command );
				SetSystemStatus( command );
				break;
			case SYSTEM_CMD_REBOOT :
				INFO_YPRINTF( "CMD: %s tid: %d\n", pResource->name, pResource->tid );
				INFO_YPRINTF( "CMD: status %d => ommand %d => SYSTEM_CMD_REBOOT\n", GetSystemStatus(), command );
				SetSystemStatus( command );
				//INFO_YPRINTF( "CMD: kill %s pid: %d\n", pResource->name, pResource->pid );
				//kill( pResource->pid, SIGKILL );
				//RebootSystem();
				break;
			case SYSTEM_CMD_POWER_DOWN :
				INFO_YPRINTF( "CMD: %s tid: %d\n", pResource->name, pResource->tid );
				INFO_YPRINTF( "CMD: status %d => ommand %d => SYSTEM_CMD_POWER_DOWN\n", GetSystemStatus(), command );
				SetSystemStatus( command );
				//INFO_YPRINTF( "CMD: kill %s pid: %d\n", pResource->name, pResource->pid );
				//kill( pResource->pid, SIGKILL );
				//ShutdownSystem();
				break;
			case SYSTEM_CMD_CLEAR_REBOOT_COUNT :
				INFO_YPRINTF( "CMD: %s tid: %d\n", pResource->name, pResource->tid );
				INFO_YPRINTF( "CMD: status %d => ommand %d => SYSTEM_CMD_CLEAR_REBOOT_COUNT\n", GetSystemStatus(), command );
				UpdateRebootStatusFile( REBOOT_BY_ALL, REBOOT_REASON_CMD_CLEAR );
				break;
			default:
				break;
		}
	}

	node.resource.status = RESOURCE_STATUS_WAIT_HEARTBEAT;
	//node.resource.command = SYSTEM_CMD_NONE;
	node.resource.isAlive = true;

	MutexLock( &_Mutex );

	if( UpdateNode( pList, &node ) == false )
	{
		ERROR_PRINTF( "DAEMON_CDR: UpdateNode() failed!!\n" );
		MutexUnlock( &_Mutex );
		return false;
	}

	MutexUnlock( &_Mutex );

	//INFO_PRINTF( "%d: ALIVE: %s pid: %d tid: %d\n", GetNodeCount( pList ), pResource->name, pResource->pid, pResource->tid );

	return true;
}

#if 0
typedef struct RESOURCE_S
{
	pid_t				pid;
	tid_t				tid;
	char				name[ RESOURCE_NAME_LEN ];
	RESOURCE_STATUS		status;		//	used in daemon
	SYSTEM_STATUS		msg;		//	command from application
	bool				isAlive;
	struct timeval		lastSendTime;
	long				seq;
} RESOURCE;	//	32 + ( 2 * pid_t ) + ( 2 * enum ) + bool + timeval + long = 64 bytes
#endif

const bool ParseMessage( const MSG_DATA_RESOURCE * const pMessage )
{
	const RESOURCE * const	pResource = &pMessage->resource;

	assert( pMessage != NULL );

	switch( pResource->status )
	{
		case RESOURCE_STATUS_SLEEP :
			/*	TODO: Update Resource status to RESOURCE_STATUS_SLEEP
			 *	*/
			//DEBUG_PRINTF( "SLEEP: %s pid: %d tid: %d\n", pResource->name, pResource->pid, pResource->tid );
			SleepResource( pResource );
			break;
		case RESOURCE_STATUS_REGISTER :
			{
				char strDateTime[ STR_TIME_STAMP_LEN ] = { 0, };

				MakeDateTimeString( &pResource->lastSendTime, strDateTime );

				/*	TODO: Register Resource to List
				 *	*/
				DEBUG_PRINTF( "%s | REGISTER: %s pid: %d tid: %d\n", \
						strDateTime, pResource->name, pResource->pid, pResource->tid );
				RegisterResource( pResource );
				break;
			}

		case RESOURCE_STATUS_ALIVE :
			/*	TODO: Update Resource status
			 *	status, seq, isAlive, lastSendTime
			 *	*/
			//DEBUG_PRINTF( "ALIVE: %s pid: %d tid: %d\n", pResource->name, pResource->pid, pResource->tid );
			AliveResource( pResource );
			break;
		case RESOURCE_STATUS_COMMAND :
			/*	TODO: Update Resource status
			 *	status, seq, isAlive, lastSendTime
			 *	*/
			//NFO_YPRINTF( "COMMAND: %s pid: %d tid: %d\n", pResource->name, pResource->pid, pResource->tid );
			AliveResource( pResource );
			break;
		default :
			break;
	}

	return true;
}

void RegisterTaskPid( const pid_t pid )
{
	int idx;

	assert( s_pDaemon != NULL );

	MutexLock( &_Mutex );

	for( idx = 0 ; idx < MAX_TASK ; idx++ )
	{
		if( s_pDaemon->taskPid[ idx ] == -1 )
		{
			INFO_YPRINTF( "Add [%d] pid %d\n", idx, pid );
			s_pDaemon->taskPid[ idx ] = pid;
			break;
		}
	}

	MutexUnlock( &_Mutex );

	return;
}

void DeregisterTaskPid( const pid_t pid )
{
	int idx;

	assert( s_pDaemon != NULL );

	MutexLock( &_Mutex );

	for( idx = 0 ; idx < MAX_TASK ; idx++ )
	{
		if( s_pDaemon->taskPid[ idx ] == pid )
		{
			INFO_YPRINTF( "Remove [%d] pid %d\n", idx, pid );
			s_pDaemon->taskPid[ idx ] = -1;
			break;
		}
	}

	MutexUnlock( &_Mutex );

	return;
}

void RebootSystem( void )
{
	const REBOOT_REASON		lastReason = GetLastRebootReason();

	INFO_RBPRINTF( "SYSTEM REBOOT By %s(%d) !!!!!!!!! status: %d\n",
			ReasonString( lastReason ), lastReason, s_lastStatus );
	UpdateRebootStatusFile( REBOOT_BY_NORMAL, REBOOT_REASON_CMD_INCREASE );
#ifdef	__USE_DAEMON_SHUTDOWN__
	/*	TODO: Change rebbot method to watchdog reboot.
	 *	by jyhuh 2019-03-14 12:49:09
	 *	*/
	setuid( 0 );
	reboot( LINUX_REBOOT_CMD_RESTART );

	ClosePmic();
#endif	//	__USE_DAEMON__
	return ;
}

void ShutdownSystem( void )
{
#ifndef	__USE_DAEMON_SHUTDOWN__
	INFO_RBPRINTF( "SYSTEM SHUTDOWN!!!!!!!!! status: %d\n", s_lastStatus );
	UpdateRebootStatusFile( REBOOT_BY_ALL, REBOOT_REASON_CMD_CLEAR );
#else	//	__USE_DAEMON_SHUTDOWN__
	bool checkAccIn = true;

	INFO_RBPRINTF( "SYSTEM SHUTDOWN!!!!!!!!! status: %d\n", s_lastStatus );
	UpdateRebootStatusFile( REBOOT_BY_ALL, REBOOT_REASON_CMD_CLEAR );

	if( s_lastStatus == SYSTEM_STATUS_FORCE_POWER_DOWN )
	{
		checkAccIn = false;
	}
	if( checkAccIn == true )
	{
		if( IsAccValid() == true )
		{
			INFO_RBPRINTF( "SYSTEM SHUTDOWN ==> REBOOT by ACC IN!!!!!!!!!\n" );
			RebootSystem();
		}
	}
	INFO_YBPRINTF( "PMIC: Last Acc Valid Check!!!\n" );

	if( PmicShutdown( checkAccIn ) == false )
	{
		INFO_RBPRINTF( "SYSTEM SHUTDOWN ==> REBOOT by PmicShutdown() failed!!!!!!\n" );
		RebootSystem();
	}

#endif	//	__USE_DAEMON__
	return;
}
