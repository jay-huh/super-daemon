#include	"debug.h"

#include	<time.h>		//	strftime

#include	<sys/syscall.h>	//	__NR_gettid

#ifdef __cplusplus
extern "C" {
#endif	//	__cplusplus

long int gettid(void)
{
	return ( long int )syscall( __NR_gettid );
}

void MakeDateTimeString( const struct timeval * const pTv, char *pStrTime )
{
	struct tm		*pTm = NULL;
	char			strTime[ STR_TIME_STAMP_LEN ] = { 0, };

	assert( ( pTv != NULL ) && ( pStrTime != NULL ) );

	pTm = localtime( &pTv->tv_sec );

	/* Format the date and time, down to a single second. */
	strftime( strTime, sizeof( strTime ), "%Y-%m-%d %H:%M:%S", pTm );

	snprintf( pStrTime, STR_TIME_STAMP_LEN, "%s.%06ld", strTime, pTv->tv_usec );

	return;
}

void MakeTimeStamp( const struct timeval * const pTv, char *pStrTime )
{
	struct tm		*pTm = NULL;
	char			strTime[ STR_TIME_STAMP_LEN ] = { 0, };

	assert( ( pTv != NULL ) && ( pStrTime != NULL ) );

	pTm = localtime( &pTv->tv_sec );

	/* Format the date and time, down to a single second. */
	strftime( strTime, sizeof( strTime ), "%H:%M:%S", pTm );

	snprintf( pStrTime, STR_TIME_STAMP_LEN, "%s.%06ld", strTime, pTv->tv_usec );

	return;
}

void time_stamp( void )
{
	struct timeval	tv;
	char			strTime[ STR_TIME_STAMP_LEN ] = { 0, };

	/* Obtain the time of day, and convert it to a tm struct. */
	gettimeofday (&tv, NULL);

	MakeTimeStamp( &tv, strTime );
	printf( "%s |", strTime );

	return;
}
#ifdef __cplusplus
}
#endif	//	__cplusplus
