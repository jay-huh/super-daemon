#include	"reboot_file.h"

#include	<string.h>	//	memset
#include	<fcntl.h>

#include	<errno.h>

#include	"debug.h"

static REBOOT_STATUS	rebootStatus =
{
	.valid = false,
	.updated = false,
	.lastReason = REBOOT_BY_NORMAL,
	.count = { 0, }
};

//static const char * ReasonString( const REBOOT_REASON reason );
static void PrintRebootStatus( void );

const char * ReasonString( const REBOOT_REASON reason )
{
	switch( reason )
	{
		case REBOOT_BY_NORMAL		: return "NORMAL";
		case REBOOT_BY_WATCHDOG		: return "WATCHDOG";
		default: return "N/A";
			break;
	}
	return "";
}

const bool IsUpdatedRebootStatusFile( void )
{
	REBOOT_STATUS * const pStatus = &rebootStatus;

	assert( pStatus != NULL );

	return pStatus->updated;
}

//const int GetRebootStatusFile( REBOOT_STATUS * const pStatus )
const int GetRebootStatusFile( void )
{
	int		fd = -1;
	int		flags = O_RDWR | O_SYNC;
	ssize_t len = -1;

	REBOOT_STATUS * const pStatus = &rebootStatus;

	assert( pStatus != NULL );

	memset( pStatus, 0x0, sizeof( REBOOT_STATUS ) );

	if( access( REBOOT_STATUS_FILE, F_OK ) < 0 )
	{
		flags |= O_CREAT;
		INFO_PRINTF( "CREATE '%s'\n", REBOOT_STATUS_FILE );
	}

	fd	= open( REBOOT_STATUS_FILE, flags, S_IRUSR | S_IWUSR );
	if( fd < 0 )
	{
		ERROR_PRINTF( "open( %s ) errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

	len = read( fd, pStatus, sizeof( REBOOT_STATUS ) );
	if( len < 0 )
	{
		ERROR_PRINTF( "read() %s: errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

	pStatus->valid = true;
	pStatus->updated = false;

	if( close( fd ) < 0 )
	{
		ERROR_PRINTF( "close( %s ) errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

#if 0
	/*	NOTE: Check for NORMAL Boot		*/
	if( ( pStatus->count[ REBOOT_BY_FRONT_CAPTURE_ERR ] == 0 )
			&& ( pStatus->count[ REBOOT_BY_REAR_CAPTURE_ERR ] == 0 ) )
	{
		if( pStatus->lastReason != REBOOT_BY_NORMAL )
		{
			UpdateRebootStatusFile( REBOOT_BY_NORMAL, REBOOT_REASON_CMD_INCREASE );
		}
	}
#endif
	PrintRebootStatus();

	return 0;
}

void PrintRebootStatus( void )
{
	int i;
	REBOOT_STATUS * const pStatus = &rebootStatus;

	INFO_BPRINTF( "REBOOT_STATUS: vaild: %d updated: %d\n", pStatus->valid, pStatus->updated );
	INFO_BPRINTF( "REBOOT_STATUS: lastReason: %s(%d)\n", ReasonString( pStatus->lastReason ), pStatus->lastReason );

	for( i = 0 ; i < REBOOT_BY_END ; i++ )
	{
		INFO_PRINTF( "REBOOT_STATUS: count[ %d ] %s: %d\n", i, ReasonString( i ), pStatus->count[ i ] );
	}
	return ;
}

const int GetRebootCount( const REBOOT_REASON reason )
{
	assert( ( reason >= REBOOT_BY_NORMAL ) && ( reason < REBOOT_BY_END ) );
	assert( rebootStatus.valid == true );

	return rebootStatus.count[ reason ];
}

const int SetRebootCount( const REBOOT_REASON reason, const int count )
{
	assert( ( reason >= REBOOT_BY_NORMAL ) && ( reason < REBOOT_BY_END ) );
	assert( rebootStatus.valid == true );

	rebootStatus.count[ reason ] = count;

	return rebootStatus.count[ reason ];
}

const REBOOT_REASON GetLastRebootReason( void )
{
	assert( rebootStatus.valid == true );

	return rebootStatus.lastReason;
}

const REBOOT_REASON SetLastRebootReason( const REBOOT_REASON reason )
{
	assert( ( reason >= REBOOT_BY_NORMAL ) && ( reason < REBOOT_BY_END ) );
	assert( rebootStatus.valid == true );

	rebootStatus.lastReason = reason;

	return rebootStatus.lastReason;
}

const bool UpdateRebootStatusFile( const REBOOT_REASON reason, const REBOOT_REASON_CMD cmd )
{
	int		fd = -1;
	int		flags = O_RDWR | O_SYNC;
	ssize_t len = -1;

	int count;
	bool poweroff = false;

	assert( ( ( reason >= REBOOT_BY_NORMAL ) && ( reason < REBOOT_BY_END ) )
			|| ( reason == REBOOT_BY_ALL ) );
	assert( ( cmd >= REBOOT_REASON_CMD_CLEAR ) && ( cmd < REBOOT_REASON_CMD_END ) );

#if 0
	if( rebootStatus.updated == true )
	{
		INFO_YBPRINTF( "Already Updated....\n" );
		PrintRebootStatus();
		return false;
	}
#endif

	if( access( REBOOT_STATUS_FILE, F_OK ) < 0 )
	{
		flags |= O_CREAT;
		INFO_PRINTF( "CREATE '%s'\n", REBOOT_STATUS_FILE );
	}

	fd	= open( REBOOT_STATUS_FILE, flags, S_IRUSR | S_IWUSR );
	if( fd < 0 )
	{
		ERROR_PRINTF( "open( %s ) errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

	count = rebootStatus.count[ reason ];
	switch( cmd )
	{
		case REBOOT_REASON_CMD_CLEAR :
			count = 0;
			break;
		case REBOOT_REASON_CMD_INCREASE :
			rebootStatus.lastReason = reason;
			count++;
			break;
		default :
			break;
	}

	/*	NOTE:
	 *	Front sensor: poweroff
	 *	Rear sensor: forced disable.
	 *	*/
	if( count > REBOOT_COUNT_POWER_OFF )
	{
		switch( reason )
		{
			case REBOOT_BY_WATCHDOG :
			case REBOOT_BY_FRONT_CAPTURE_ERR :
			WARN_PRINTF( "reason: %s(%d) count %d => %d => POWEROFF\n",
					ReasonString( reason ), reason, rebootStatus.count[ reason ], count );
				poweroff = true;
				//count = 0;
				break;
			case REBOOT_BY_REAR_CAPTURE_ERR :
				count = REBOOT_COUNT_FORCED_DISABLE;
				break;
			default :
				//count = 0;
				break;
		}
	}

	/*	update reboot status	*/
	INFO_BPRINTF( "REBOOT reason: %s(%d) count %d => %d\n",
			ReasonString( reason ), reason, rebootStatus.count[ reason ], count );

	if( reason == REBOOT_BY_ALL )
	{
		int i = 0;
		for( i = 0 ; i < REBOOT_BY_END ; i++ )
		{
			rebootStatus.count[ i ] = count;
		}
	}
	else
	{
		rebootStatus.count[ reason ] = count;
	}

	lseek( fd, 0, SEEK_SET );

	len = write( fd, &rebootStatus, sizeof( REBOOT_STATUS ) );
	if( len < 0 )
	{
		ERROR_PRINTF( "write() %s: errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

	if( close( fd ) < 0 )
	{
		ERROR_PRINTF( "close( %s ) errno(%d) %s\n",
				REBOOT_STATUS_FILE, errno, strerror( errno ) );
		return -1;
	}

	rebootStatus.updated = true;

	PrintRebootStatus();

	return poweroff;
}

