#include	"list.h"

#include	"mutex.h"

#include	"debug.h"

static NODE * const FindNodeByPid( LIST * const pList, const pid_t * const pPid );
static NODE * const FindNodeByTid( LIST * const pList, const tid_t * const pTid );
static NODE * const FindNodeByStatus( LIST * const pList, const RESOURCE_STATUS * const pStatus );

const bool DeleteNodeByField( LIST * const pList, const RESOURCE_FIELD field, const void * const pData )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( ( field >= RESOURCE_FIELD_PID ) && ( field < RESOURCE_FIELD_END ) );

	switch( field )
	{
		case RESOURCE_FIELD_PID :
			INFO_PRINTF( "LIST: DeleteNodeByField() field %d pid: %d\n", field, *( ( pid_t * )pData ) );
			break;
		case RESOURCE_FIELD_TID :
			INFO_PRINTF( "LIST: DeleteNodeByField() field %d tid: %d\n", field, *( ( tid_t * )pData ) );
			break;
		default:
			WARN_PRINTF( "LIST: DeleteNodeByField() field %d Not supportted", field );
			return false;
	}

	pIter = FindNodeByField( pList, field, pData );

	if( pIter == NULL ) return false;

	return DeleteNode( pList, pIter );
}

NODE * const FindNodeByField( LIST * const pList, const RESOURCE_FIELD field, const void * const pData )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( ( field >= RESOURCE_FIELD_PID ) && ( field < RESOURCE_FIELD_END ) );

	switch( field )
	{
		case RESOURCE_FIELD_PID :
			{
				pIter = FindNodeByPid( pList, ( pid_t * )pData );
				break;
			}
		case RESOURCE_FIELD_TID :
			{
				pIter = FindNodeByTid( pList, ( tid_t * )pData );
				break;
			}
		case RESOURCE_FIELD_STATUS :
			{
				pIter = FindNodeByStatus( pList, ( RESOURCE_STATUS * )pData );
				break;
			}
		default:
			break;
	}

	return pIter;
}

NODE * const FindNodeByPid( LIST * const pList, const pid_t * const pPid )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( pPid != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter->resource.pid == *pPid )
		{
			break;
		}
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const FindNodeByTid( LIST * const pList, const pid_t * const pTid )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( pTid != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter->resource.tid == *pTid )
		{
			break;
		}
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return pIter;
}

NODE * const FindNodeByStatus( LIST * const pList, const RESOURCE_STATUS * const pStatus )
{
	NODE *pIter = NULL;

	assert( pList != NULL );
	assert( pStatus != NULL );

	MutexLock( &pList->mutex );

	pIter = pList->pHead;

	while( pIter != NULL )
	{
		if( pIter->resource.status == *pStatus )
		{
			break;
		}
		pIter = pIter->pNext;
	}

	MutexUnlock( &pList->mutex );

	return pIter;
}
