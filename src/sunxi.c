#include	"sunxi.h"

#include	<string.h>
#include	<errno.h>

#include	<sys/ioctl.h>
#include	<fcntl.h>

#include	<linux/watchdog.h>

#include	"debug.h"

static int sunxiFd = -1;
static int wdtFd = -1;


static const bool RegRead( const int reg, unsigned int * const pData );
static const bool RegWrite( const int reg, const unsigned int * const pData );

static const int PmicContol( const int mode, void * const pData );
static const bool LedPoweOff( void );

const int OpenDevice( const char * const pDevFile, const int flags )
{
	int fd = -1;

	assert( pDevFile != NULL );

	fd	= open( pDevFile, flags );
	if( fd < 0 )
	{
		ERROR_PRINTF( "open( %s ) errno(%d) %s\n", pDevFile, errno, strerror( errno ) );
		return -1;
	}

	return fd;
}

const bool InitWatchdog( void )
{
	int timeout = WDT_TIMEOUT;
#ifdef	__USE_DAEMON_SHUTDOWN__
	int ret = -1;

	assert( wdtFd < 0 );

	wdtFd = OpenDevice( "/dev/watchdog", O_RDWR );
	if( wdtFd < 0 )
	{
		ERROR_PRINTF(  "OpenDevice error.. sunxiFd: %d\n", wdtFd );
		return false;
	}

	ret = ioctl( wdtFd, WDIOC_SETTIMEOUT, &timeout );
	if( ret < 0 )
	{
		ERROR_PRINTF( "WDT: ioctl( WDIOC_SETTIMEOUT ) errno(%d) %s\n", errno, strerror( errno ) );
		return false;
	}
	ret = ioctl( wdtFd, WDIOC_GETTIMEOUT, &timeout );
#endif	//	__USE_DAEMON_SHUTDOWN__
	INFO_PRINTF( "WDT: Init Timeout: %d seconds.\n", timeout );

	return KeepAliveWatchdog();
}

const bool UpdateWatchdog( const int timeout )
{
#ifdef	__USE_DAEMON_SHUTDOWN__
	int ret = -1;

	assert( wdtFd >= 0 );

	ret = ioctl( wdtFd, WDIOC_SETTIMEOUT, &timeout );
	if( ret < 0 )
	{
		ERROR_PRINTF( "WDT: ioctl( WDIOC_SETTIMEOUT ) errno(%d) %s\n", errno, strerror( errno ) );
		return false;
	}
#endif	//	__USE_DAEMON_SHUTDOWN__
	INFO_PRINTF( "WDT: Update Timeout: %d seconds.\n", timeout );

	return true;
}

const int GetWatchdogLeftTime( void )
{
	int leftTime = 0;
#ifdef	__USE_DAEMON_SHUTDOWN__
	int ret = -1;

	assert( wdtFd >= 0 );

	ret = ioctl( wdtFd, WDIOC_GETTIMELEFT, &leftTime );
	if( ret < 0 )
	{
		ERROR_PRINTF( "WDT: ioctl( WDIOC_GETTIMELEFT ) errno(%d) %s\n", errno, strerror( errno ) );
		return false;
	}
#endif	//	__USE_DAEMON_SHUTDOWN__
	INFO_PRINTF( "WDT: Left time to expire: %d seconds.\n", leftTime );

	return leftTime;
}

const bool CloseWatchdog( void )
{
	if( wdtFd < 0 ) return true;

	close( wdtFd );

	return true;
}

const bool KeepAliveWatchdog( void )
{
#ifdef	__USE_DAEMON_SHUTDOWN__
	int ret = -1;

	assert( wdtFd >= 0 );

	ret = ioctl( wdtFd, WDIOC_KEEPALIVE, 0 );
	if( ret < 0 )
	{
		ERROR_PRINTF( "WDT: ioctl( WDIOC_KEEPALIVE ) errno(%d) %s\n", errno, strerror( errno ) );
		return false;
	}
	//IsAccValid();
#else	//	__USE_DAEMON_SHUTDOWN__
	INFO_PRINTF( "WDT: KeepAlive....\n" );
#endif	//	__USE_DAEMON_SHUTDOWN__
	return true;
}

const bool InitPmic( void )
{
#ifdef	__USE_DAEMON_SHUTDOWN__
	bool isValid = false;
	PMIC_RW		pmicRw = { 0x95, 0x0 };

	assert( sunxiFd < 0 );

	sunxiFd = OpenDevice( "/dev/sunxi-reg", O_RDWR );
	if( sunxiFd < 0 )
	{
		ERROR_PRINTF(  "OpenDevice error.. sunxiFd: %d\n", sunxiFd );
		return false;
	}

	/*	Set PMIC GPIO_3 to Input.	*/
	PmicContol( SUNXI_GET_PMIC, &pmicRw );
	pmicRw.data |= ( 0x1 << 2 );
	PmicContol( SUNXI_SET_PMIC, &pmicRw );

	INFO_PRINTF( "PMIC: SET: reg: 0x%02x data: 0x%02x\n", pmicRw.addr, pmicRw.data );

	/*	ACC Detection	*/
	PmicContol( SUNXI_GET_PMIC, &pmicRw );
	if( ( pmicRw.data & 0x01 ) == 0x00 )	//	0x00: Input Low
	{
		isValid = true;
	}
	INFO_YBPRINTF( "PMIC: ACC: reg: 0x%02x data: 0x%02x isValid: %d\n", pmicRw.addr, pmicRw.data, isValid );

#else	//	__USE_DAEMON_SHUTDOWN__
	INFO_PRINTF( "PMIC: Init....\n" );
#endif	//	__USE_DAEMON_SHUTDOWN__

	return true;
}

const bool ClosePmic( void )
{
	if( sunxiFd < 0 ) return true;

	close( sunxiFd );

	return true;
}

const bool RegRead( const int reg, unsigned int * const pData )
{
	int ret = 0;

	assert( pData != NULL );
	assert( sunxiFd >= 0 );

	do
	{
		ret = lseek( sunxiFd, reg, sizeof( unsigned int ) );
		if( ret < 0 )
		{
			ERROR_PRINTF( "lseek() reg: 0x%08x errno(%d) %s\n",
				reg, errno, strerror( errno ) );
		}
	 }while( ret < 0 );

	do
	{
		ret = read( sunxiFd, pData, sizeof( unsigned int ) );
		if( ret < 0 )
		{
			ERROR_PRINTF( "read() reg: 0x%08x errno(%d) %s\n",
				reg, errno, strerror( errno ) );
		}
	}while( ret < 0 );

	//DEBUG_PRINTF( "Read: reg: 0x%08x data: 0x%08x\n", reg, *pData );

	return true;
}

const bool RegWrite( const int reg, const unsigned int * const pData )
{
	int ret = 0;

	assert( pData != NULL );
	assert( sunxiFd >= 0 );

	do
	{
		ret = lseek( sunxiFd, reg, sizeof( unsigned int ) );
		if( ret < 0 )
		{
			ERROR_PRINTF( "lseek() reg: 0x%08x errno(%d) %s\n",
				reg, errno, strerror( errno ) );
		}
	 }while( ret < 0 );

	do
	{
		ret = write( sunxiFd, pData, sizeof( unsigned int ) );
		if( ret < 0 )
		{
			ERROR_PRINTF( "write() reg: 0x%08x errno(%d) %s\n",
				reg, errno, strerror( errno ) );
		}
	}while( ret < 0 );

	//DEBUG_PRINTF( "Write: reg: 0x%08x data: 0x%08x\n", reg, *pData );

	return true;
}

const int PmicContol( const int mode, void * const pData )
{
	int ret = 0;
	struct pmic_rw *pRw = NULL;
	struct pmic_info *pInfo = NULL;
	int *pCache = NULL;

	assert( pData != NULL );
	//assert( sunxiFd >= 0 );
	if( sunxiFd < 0 )
	{
		ERROR_PRINTF( "mode: %d( 0x%08x ).. \n", mode, mode );
		return -1;
	}

	if( mode == SUNXI_SET_PMIC || mode == SUNXI_GET_PMIC )
	{
		pRw = (struct pmic_rw *)pData;
	}
	else if(mode == SUNXI_GET_PMIC_INFO)
	{
		pInfo = (struct pmic_info *)pData;
	}
	else if(mode == SUNXI_SET_CACHE_CONTROL)
	{
		pCache = (int *)pData;
	}
	else
	{
		ERROR_PRINTF( "mode: %d( 0x%08x ).. not support.. \n", mode, mode );
		return -1;
	}

	do
	{
		if(mode == SUNXI_SET_PMIC || mode == SUNXI_GET_PMIC)
			ret = ioctl( sunxiFd, mode, pRw);
		else if(mode == SUNXI_GET_PMIC_INFO)
			ret = ioctl( sunxiFd, mode, pInfo);
		else
			ret = ioctl( sunxiFd, mode, pCache);
		if( ret < 0 )
		{
			ERROR_PRINTF( "ioctl() mode: %d( 0x%08x) errno(%d) %s\n",
				mode, mode, errno, strerror( errno ) );
		}
	}while(ret < 0);

	return 0;
}

const bool PmicShutdown( const bool checkAccIn )
{
	/*	0x32: Shutdown Register
	 *	0x84: PMIC Shutdown with reverse sequenceto startup
	 *	by jyhuh 2019-03-10 22:47:25
	 *	*/
	//PMIC_RW		pmicRw = { 0x32, 0x84 };
	PMIC_RW		pmicRw = { 0x32, 0x80 };

	INFO_YBPRINTF( "SUNXI: PMIC Shutdown!!!!!!\n" );

#if 0	//	NOTE: PMIC Information...
	PMIC_INFO	pmicInfo;
	PMIC_RW		reg = { 0x0, 0x0 };
	PmicContol( sunxiFd, SUNXI_GET_PMIC, &reg );
	INFO_YBPRINTF( "SUNXI: PMIC reg 0x%2x value: 0x%2x\n", reg.addr, reg.data );

	PmicContol( sunxiFd, SUNXI_GET_PMIC_INFO, &pmicInfo );

	INFO_YBPRINTF( "SUNXI: PMIC ac_det: %d ac_valid: %d carbat: %d\n",
			pmicInfo.ac_det, pmicInfo.ac_valid, pmicInfo.carbat );

#endif
	LedPoweOff();

	//sleep( 3 );	//	for debug by jyhuh 2019-03-11 22:07:10

	if( checkAccIn == true )
	{
		if( IsAccValid() == true ) return false;
	}

	PmicContol( SUNXI_SET_PMIC, &pmicRw );

	return true;
}

const bool IsAccValid( void )
{
	/*	INFO: ACC_DETECT
	 *	- Pin assignment: GPIO_3 of PMIC
	 *	- Reg. 0x95
	 *	- ACC ON : LOW
	 *	by jyhuh 2019-03-14 12:26:02
	 *	*/
	PMIC_RW		pmicRw = { 0x95, 0x0 };
	bool isValid = false;

	PmicContol( SUNXI_GET_PMIC, &pmicRw );

	if( ( pmicRw.data & 0x01 ) == 0x00 )	//	0x00: Input Low
	{
		isValid = true;
	}
	//INFO_PRINTF( "PMIC: ACC: reg: 0x%02x data: 0x%02x isValid: %d\n", pmicRw.addr, pmicRw.data, isValid );

	return isValid;
}

const bool LedPoweOff( void )
{
	unsigned int reg, data;

	assert( sunxiFd >= 0 );

	INFO_YBPRINTF( "SUNXI: LCD Poweroff!!!!!!\n" );

	//case CDRHPR_SXIDBG_LCD_POWEROFF:
	//CVBS_EN PE03, CVBS_RSTN PC09
	//PE03
	reg = 0x1c20800 + 0xA0;
	RegRead( reg, &data);
	data = data & ~0x00000008;
	RegWrite( reg, &data);
	//usleep( 10 * 1000);

	//case CDRHPR_SXIDBG_LCD_ONOFF:
	reg = 0x01c21400;
	RegRead( reg, &data);
	data = data & ~0x00000010; //PWM_CH0_EN disable
	RegWrite( reg, &data);

	//case CDRHPR_SXIDBG_GPS_ONOFF:
	//gps power -> pe17
	reg = 0x1c20800 + 0xA0;
	RegRead( reg, &data);
	data = data & ~0x00020000;
	RegWrite( reg, &data);

	//case CDRHPR_SXIDBG_REARCAM_ONOFF:
	//PE02
	reg = 0x01c20800+0xA0;
	RegRead( reg, &data );
	data = data & ~0x00000004;
	RegWrite( reg, &data );

	//case CDRHPR_SXIDBG_LED_REC: //PE18
	reg = 0x1c20800 + 0xA0;
	RegRead( reg, &data );
	data = data & ~0x00040000;
	RegWrite( reg, &data );

	//case CDRHPR_SXIDBG_LED_GPS: //PE19
	reg = 0x1c20800 + 0xA0;
	RegRead( reg, &data );
	data = data & ~0x00080000;
	RegWrite( reg, &data );

	//case CDRHPR_SXIDBG_LED_STATUS: //PF06
	reg = 0x1c20800 + 0xc4; 
	RegRead( reg, &data );
	data = data & ~0x00000040;
	RegWrite( reg, &data );

	return true;
#if 0
		case CDRHPR_SXIDBG_REARCAM_ONOFF:
		{
			//PE02
			int *pIsOn = (int*)input;
			DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_REARCAM_ONOFF received... %d\n", *pIsOn);
			
			reg = 0x01c20800+0xA0;
			reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);
			
			wdata = rdata & ~0x00000004;
			if(*pIsOn)
				wdata = rdata | 0x00000004;
			
			reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
		}
			break;
		case CDRHPR_SXIDBG_LCD_ONOFF:
			if( input!=NULL )
			{
				int *pIsOn = (int*)input;
				DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LCD_ONOFF received...IsOn: %d\n", *pIsOn);
				
				reg = 0x01c21400;
				reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);

				wdata = rdata & ~0x00000010; //PWM_CH0_EN disable
				if(*pIsOn)
					wdata = rdata | 0x00000010; //PWM_CH0_EN enable

				reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			}
			else
			{
				ret = -1;
			}
			break;
		case CDRHPR_SXIDBG_LCD_POWEROFF:
		{
			DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LCD_POWEROFF\n");
#ifdef ALPINE
			//CVBS_EN PE03, CVBS_RSTN PC09
			//PE03
			reg = 0x1c20800 + 0xA0;
			reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);
			wdata = rdata & ~0x00000008;
			reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			//DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"PE03: r - 0x%x, w - 0x%x\n", rdata, wdata);
			usleep(10000);
#else
			//PC09
/*
			reg = 0x1c20800 + 0x4C;
			reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);
			wdata = rdata & ~0x000000F0;
			reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LCD_POWEROFF 0x4C -> 0x%x\n", wdata);
*/
			reg = 0x1c20800 + 0x58;
			reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);
			wdata = rdata & ~0x00000200;
			reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			//DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LCD_POWEROFF 0x58 -> 0x%x\n", wdata);
#endif
		}
		case CDRHPR_SXIDBG_GPS_ONOFF:
		{
#ifndef ALPINE
			int *pgpsonoff = (int*)input;
			DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_GPS_ONOFF pgpsonoff: %d...\n", *pgpsonoff);
			//gps power -> pe17
			reg = 0x1c20800 + 0xA0;
			reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);
			
			wdata = rdata & ~0x00020000;
			if(*pgpsonoff)
				wdata = rdata | 0x00020000;
			
			reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
#endif
		}
		break;

		case CDRHPR_SXIDBG_LED_REC: //PE18
		{
			//DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LED_REC received...\n");
#if defined(CARNAVICOM) || defined(ALPINE)
			if(led_start == 0)
				break;
#endif

			if(input != NULL)
			{
				int *pIsOn = (int*)input;

				reg = 0x1c20800 + 0xA0;
				reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);

				wdata = rdata & ~0x00040000;
				if(*pIsOn)
					wdata = rdata | 0x00040000;

				reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			}
			else
			{
				ret = -1;
			}
		}
		break;

		case CDRHPR_SXIDBG_LED_GPS: //PE19
		{
#if defined(ALPINE) || defined(VG900V3)
			//DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LED_GPS received...\n");
			if(input != NULL)
			{
				int *pIsOn = (int*)input;

				reg = 0x1c20800 + 0xA0;
				reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);

				wdata = rdata & ~0x00080000;
				if(*pIsOn)
					wdata = rdata | 0x00080000;

				reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			}
			else
			{
				ret = -1;
			}
#endif
		}
		break;
		case CDRHPR_SXIDBG_LED_STATUS: //PF06
		{
			//DBG_M(DBG_THX_TOP,DBG_LEV_INFO,BROWN,"CDRHPR_SXIDBG_LED_STATUS received...\n");
#if defined(CARNAVICOM) || defined(ALPINE)
			if(led_start == 0)
				break;
			
			if( input!=NULL )
			{
				int *pIsOn = (int*)input;
				
				reg = 0x1c20800 + 0xc4; 
				reg_read(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &rdata);

				wdata = rdata & ~0x00000040;
				if(*pIsOn){
					wdata = rdata | 0x00000040;
				}

				reg_write(cdr_helper.sunxiFd_sunxi_dbgreg, reg, &wdata);
			}
			else
			{
				ret = -1;
			}
#endif
		}
		break;
#endif
}

